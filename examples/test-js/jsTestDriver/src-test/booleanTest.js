booleanTest2 = TestCase("booleanTest2");

$("#main").append('<input id="valueElement" type="checkbox"/>'
        + '<button id="updateButton" onclick=refresh_resource(ws)>Set Status</button>');

booleanTest2.prototype.testGetFalse = function(){
    $("#valueElement").attr('checked', true);
    var ws = new WebSocket('ws://echo.websocket.org');
    $("#updateButton").click(refresh_resource(ws));
    
    jstestdriver.setTimeout(function(){
        assertFalse($("#valueElement").is('checked'));
    }, 1000);
};

booleanTest2.prototype.testSetTrue = function(){
    $("#valueElement").attr('checked', false);
    var ws = new WebSocket('ws://echo.websocket.org');
    $("#updateButton").click(set_resource(ws, true));
    jstestdriver.setTimeout(function(){
    	assertFalse($("#valueElement").is('checked'));
    }, 1000);
};
