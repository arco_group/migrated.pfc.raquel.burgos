booleanTest = TestCase("booleanTest");

$("#main").append('<input id="valueElement" type="checkbox"/>'
        + '<button id="updateButton" onclick=refresh_resource(ws)>Set Status</button>');

booleanTest.prototype.testGetTrue = function(){
    var ws = new WebSocket('ws://echo.websocket.org');
    $("#valueElement").attr('checked', false);
    $("#updateButton").click(refresh_resource(ws));
    jstestdriver.setTimeout(function(){
        assertTrue($("#valueElement").is(':checked'));
    }, 1000);
};