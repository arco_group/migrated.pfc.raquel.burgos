
function set_resource(ws, value) {

    if (! 'WebSocket' in window) {
    	alert('Tu navegador no soporta Websockets');
    	return;
    }
    //var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#valueElement').attr('href'));
    ws.onopen = function() {
	jstestdriver.console.log("va a enviar un set: "+value);
        ws.send(value);
    };
    ws.onmessage = function (message) {
        var data = message.data;
        jstestdriver.console.log(data);
        $('#valueElement').attr('checked', data=='true');
    };
};

function refresh_resource(ws) {

    if (! 'WebSocket' in window) {
    	alert('Tu navegador no soporta Websockets');
    	return;
    }
    //var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#valueElement').attr('href'));
    ws.onopen = function() {
	jstestdriver.console.log("va a enviar un get");
        ws.send('get:');
    };
    ws.onmessage = function (message) {
        var data = message.data;
	jstestdriver.console.log("llega: " + data);
	if (data != 'true'){
            $('#valueElement').attr('checked', false);
	}
    };
}
