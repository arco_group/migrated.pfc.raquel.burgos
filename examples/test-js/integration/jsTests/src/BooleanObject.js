//$(document).ready(refresh_resource());

function set_resource() {
	if (! 'WebSocket' in window) {
        alert('Tu navegador no soporta Websockets');
        return;
    }
    var ws = new WebSocket('ws://localhost:8090/inspect-element/?proxy='+$('#valueElement').attr('href'));

    ws.onopen = function() {
    	jstestdriver.console.log("Va a enviar un set:" + $('#valueElement').is('checked'));
        ws.send('set:' + $('#valueElement').is('checked'));
    };
    ws.onmessage = function (message) {
        var data = message.data;
        jstestdriver.console.log("Llega algo: " +$('#valueElement').attr('checked'));
        $('#valueElement').attr('checked', data=='true');
   };
};

function refresh_resource() {
    if (! 'WebSocket' in window) {
        alert('Tu navegador no soporta Websockets');
        return;
    }
    var ws = new WebSocket('ws://localhost:8090/inspect-element/?proxy='+$('#valueElement').attr('href'));

    ws.onopen = function() {
        ws.send('get:');
        jstestdriver.console.log("Envia el get");
    };
    ws.onmessage = function (message) {
        var data = message.data;
        $('#valueElement').attr('checked', data=='true');
    };
}
