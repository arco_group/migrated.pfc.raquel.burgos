QueueTest = AsyncTestCase("QueueTest");

QueueTest.prototype.setUp = function() {
	
	$("body").append("<input class='checkbox' type='checkbox' id='valueElement' name='Hello1' " +
			"href='Hello1 -t:tcp -h 127.0.0.1 -p 8889' onClick = set_resource()/>" +
			"<button class='button' id='updateButton' onClick = refresh_resource()></button>");
};

QueueTest.prototype.testGetTrue = function(queue) {
	  queue.call('Paso 1: hacer la llamada al refresh', function() {
		  $("#updateButton").click();
	  });

	  queue.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { });
		  window.setTimeout(myCallback, 1000);
	  });

	  queue.call('Paso 3: comprobar que el valor es true', function() {
		  jstestdriver.console.log("ahora tiene: "+$("#valueElement").is(":checked"));
		  assertTrue($('#valueElement').is(':checked'));
	  });
};

QueueTest.prototype.testSetFalse = function(queue2) {
	  queue2.call('Paso 1: hacer la llamada al set', function() {
		  $("#valueElement").checked=true;
		  jstestdriver.console.log("ha hecho click y ahora tiene: "+$("#valueElement").is(":checked"));
	  });

	  queue2.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { 
		  });
		  window.setTimeout(myCallback, 1000);
	  });

	  queue2.call('Paso 3: comprobar que el valor es false', function() {
		  assertFalse($('#valueElement').is(':checked'));
	  });
};