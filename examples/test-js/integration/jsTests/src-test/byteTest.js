ByteTest = AsyncTestCase("ByteTest");

ByteTest.prototype.setUp = function() {
	
	$("body").append("<input class='textbox' type='textbox' id='value' name='Byte1' " +
			"href='Byte1 -t:tcp -h 127.0.0.1 -p 8892'/>" +
			'<button class="button" id="validateButton" name="validateButton" onClick=set_byte()>Validar</button>'+
			"<button class='button' id='updateButton' onClick = refresh_byte()>Actualizar</button>");
};

ByteTest.prototype.testGet15 = function(queue) {
	  queue.call('Paso 1: hacer la llamada al refresh', function() {
		  $("#updateButton").click();
	  });

	  queue.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { });
		  window.setTimeout(myCallback, 250);
	  });

	  queue.call('Paso 3: comprobar que el valor es 15', function() {
		  jstestdriver.console.log("ahora tiene: "+$("#value").val());
		  assertEquals($('#value').val(), 15);
	  });
};

ByteTest.prototype.testSet45 = function(queue) {
	  queue.call('Paso 1: hacer la llamada al set', function() {
		  $('#value').val(45);
		  $("#validateButton").click();
	  });

	  queue.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { });
		  window.setTimeout(myCallback, 1000);
	  });
	  
	  queue.call('Paso 3: comprobar que el valor es 15', function() {
		  jstestdriver.console.log("ahora tiene: "+$("#value").val());
		  assertEquals($('#value').val(), 45);
	  });
};

/*ByteTest.prototype.testNotNumber = function(queue){
	queue.call('Paso 1: hacer la llamada al set', function() {
		  $('#value').val("algo");
		  $("#validateButton").click();
	  });

	  queue.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { });
		  window.setTimeout(myCallback, 1000);
	  });
	  
	  queue.call('Paso 3: comprobar que da un error', function() {
		  jstestdriver.console.log("ahora tiene: "+$("#value").val());
		  assertObject(window.alert('El objeto sólo acepta números enteros'));
	  });
};*/