
function set_resource(ws, value) {

    if (! 'WebSocket' in window) {
	alert('Tu navegador no soporta Websockets');
	return;
    }
    //var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#valueElement').attr('href'));
    ws.onopen = function() {
	console.log("va a enviar un set");
        ws.send(value);
    };
    ws.onmessage = function (message) {
        var data = message.data;
        $('#valueElement').attr('checked', data=='true');
    };
};

function refresh_resource(ws) {

    if (! 'WebSocket' in window) {
	alert('Tu navegador no soporta Websockets');
	return;
    }
    //var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#valueElement').attr('href'));
    ws.onopen = function() {
	console.log("va a enviar un get");
        ws.send('get:');
    };
    ws.onmessage = function (message) {
        var data = message.data;
	console.log(data);
	if (data != 'true'){
            $('#valueElement').attr('checked', false);
	}
    };
}
