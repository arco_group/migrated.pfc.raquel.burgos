if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}

function communicate(ws, jsonMessage) {

    ws.onopen = function() {
	//console.log(jsonMessage);
        ws.send(jsonMessage);
    };

    ws.onmessage = function (msg) {
        var data = msg.data;
	console.log(data);
	var res = jQuery.parseJSON(data);
	$("#valueElement").attr('checked', res.value);
    };
};
