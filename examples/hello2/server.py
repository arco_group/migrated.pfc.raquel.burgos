#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

from tornado import ioloop, web, websocket
import sys
import Ice

Ice.loadSlice("/usr/share/duo/slice/DUO/DUO.ice -I /usr/share/Ice/slice --all")
import DUO


class Hello1Handler(web.RequestHandler):
    def get(self):
        self.render('client.html')


class IBoolControlerFactory:
    def __init__(self, proxy):
        self.proxy = proxy

    def __call__(self, *args):
        return IBoolControler(self.proxy, *args)


class IBoolControler(websocket.WebSocketHandler):
    def __init__(self, proxy, *args):
        websocket.WebSocketHandler.__init__(self, *args)
        self.proxy = proxy

    def on_message(self, msg):
        respuesta = proxy.get()
        self.write_message(str(respuesta))



ic = Ice.initialize()
proxy = ic.stringToProxy(sys.argv[1])
proxy = DUO.IBool.RPrx.checkedCast(proxy)


application = web.Application([
    (r"/", Hello1Handler),
    (r"/inspect-hello", IBoolControlerFactory(proxy)),
    ])

application.listen(8080)
ioloop.IOLoop.instance().start()















