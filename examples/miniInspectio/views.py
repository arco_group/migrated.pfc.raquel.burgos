class ViewFactory:
    views = {}

    @classmethod
    def register(cls, model_name, view):
        cls.views[model_name] = view

    @classmethod
    def create(cls, model):
        return cls.views[model.__class__.__name__]


class View:
    def get_template(self):
        return self.template

    def get_script(self):
        s = open(self.script, 'r')
        script= s.read()
        s.close()
        return script

class BoolView(View):
    def __init__(self):
        self.template = "templates/checkbox.html"
        self.script = "templates/js/common.js"

class ByteView(View):
    def __init__(self):
        self.template = "templates/textbox.html"
        self.script = "templates/js/common.js"

ViewFactory.register('BoolModel', BoolView())
ViewFactory.register('ByteModel', ByteView())
