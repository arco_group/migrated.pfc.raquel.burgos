import logging
from tornado import ioloop, web, websocket
import models

class ControllerFactory:
    def __init__(self, ic):
        self.ic = ic

    def create(self, *args):
        return Websocket(self.ic, *args)

    def __call__(self, *args):
        return self.create(*args)


class Websocket(websocket.WebSocketHandler):
    def __init__(self, ic, *args):
        websocket.WebSocketHandler.__init__(self,*args)
        path = self.request.arguments.get('proxy')[0]
        print("Nuevo websocket: "+path)
        proxy = ic.stringToProxy(path)
        self.model = models.ModelFactory.create(proxy)
        self.model.set_controller(self)


    def on_message(self, msg):
#        method, params = algo.unmarshal(self.model, msg)
        method, param = msg.split(':')
        if method == 'get':
            status = self.model.get()
            self.write_message(status)
        else:
            self.model.set(param)
