$(document).ready(refresh_resource());
function set_resource() {
    if (! 'WebSocket' in window) {
        alert('Tu navegador no soporta Websockets');
        return;
    }
    var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#statusElement').attr('href'));
    ws.onopen = function() {
        ws.send('set:' + $('#statusElement').is(':checked'));
    };
    ws.onmessage = function (message) {
        var data = message.data;
        $('#statusElement').attr('checked', data=='true');
        };
};

function refresh_resource() {
    if (! 'WebSocket' in window) {
        alert('Tu navegador no soporta Websockets');
        return;
    }

    var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#statusElement').attr('href'));
    ws.onopen = function() {
        ws.send('get:');
        };
    ws.onmessage = function (message) {
        var data = message.data;
        $('#statusElement').attr('checked', data=='true');
    };
}
