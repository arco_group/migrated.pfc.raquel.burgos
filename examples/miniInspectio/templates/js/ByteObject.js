      $(document).ready(refresh_resource());
      function set_resource() {
	if(isNaN($('#value').val())) {
	  alert('El objeto sólo acepta números enteros');
	  return false;
	}
        if (! 'WebSocket' in window) {
          alert('Tu navegador no soporta Websockets');
          return;
        }
        var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#value').attr('href'));
        ws.onopen = function() {
          ws.send('set:' + $('#value').val());
        };
        ws.onmessage = function (message) {
          var data = message.data;
          $('#value').val(data);
        };
      }
      function refresh_resource() {
        if (! 'WebSocket' in window) {
          alert('Tu navegador no soporta Websockets');
          return;
        }
        var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#value').attr('href'));
        ws.onopen = function() {
          ws.send('get:');
        };
        ws.onmessage = function (message) {
          var data = message.data;
          $('#value').val(data);
        };
      }
