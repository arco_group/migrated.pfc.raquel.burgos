$(document).ready(refresh_resource());
function set_resource() {
    if (! 'WebSocket' in window) {
        alert('Tu navegador no soporta Websockets');
        return;
    }
    var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#valueElement').attr('href'));
    ws.onopen = function() {
	var data;
	switch($('#valueElement').attr('type')){
	    case 'checkbox':
	          data = $('#valueElement').is(':checked');
	          break;
	    case 'textbox':
	          if(isNaN($('#valueElement').val())) {
		      alert('El objeto sólo acepta números enteros');
		      return false;
		  }
	          data = $('#valueElement').val();
	          break;
	}
            ws.send('set:' + data);
    };
    ws.onmessage = function (message) {
        var data = message.data;
	switch($('#valueElement').attr('type')){
	    case 'checkbox':
	          $('#valueElement').attr('checked', data=='true');
	          break;
	    case 'textbox':
	          $('#valueElement').val(data);
	          break;
	}

    };
};

function refresh_resource() {
    if (! 'WebSocket' in window) {
        alert('Tu navegador no soporta Websockets');
        return;
    }

    var ws = new WebSocket('ws://localhost:8080/inspect-element/?proxy='+$('#valueElement').attr('href'));
    ws.onopen = function() {
        ws.send('get:');
        };
    ws.onmessage = function (message) {
        var data = message.data;
	switch($('#valueElement').attr('type')){
	    case 'checkbox':
	          $('#valueElement').attr('checked', data=='true');
	          break;
	    case 'textbox':
	          $('#valueElement').val(data);
	          break;
	}
    };
}
