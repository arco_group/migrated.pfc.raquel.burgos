#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import sys, os
import logging
logging.getLogger().setLevel(logging.DEBUG)
import codecs
import Ice, IceStorm
from tornado import ioloop, web, websocket

import models, views, controllers

Ice.loadSlice("-I /usr/share/Ice/slice /usr/share/duo/slice/DUO.ice --all")

import DUO

dummies={"Hello1": "Hello1 -t:tcp -h 127.0.0.1 -p 8889",
         #"Hello2": "Hello2 -t:tcp -h 127.0.0.1 -p 8890",
         "Byte1": "Byte1 -t:tcp -h 127.0.0.1 -p 8892"}


class ObjectDirectoryHandler(web.RequestHandler):
    def get(self):
        self.render('templates/directory.html', listAvailable=dummies)


class ObjectProxyHandler(web.RequestHandler):
    def get(self):
        path = self.request.arguments.get('proxy')[0]
        proxy = Server.ic.stringToProxy(path)
        model = models.ModelFactory.create(proxy)
        model.setup(Server.adapter)
        view = views.ViewFactory.create(model)
        pag = view.get_template()
        script = view.get_script()
        nam = str(proxy.ice_getIdentity().name)
        self.render(pag, name=nam, href=proxy, script=script)


class Server(Ice.Application, web.Application):
    def run(self, args):
        Server.ic = ic = self.communicator()
        Server.adapter = ic.createObjectAdapterWithEndpoints('InspectioAdapter', 'default')
        Server.adapter.activate()
        app = web.Application([
                (r"/$", ObjectDirectoryHandler),
                (r"/inspect-element/", controllers.ControllerFactory(Server.ic)),
                (r"/handler/", ObjectProxyHandler),
                ])
        app.listen(8090)
        print("Servidor escuchando en el puerto 8090")
        self.shutdownOnInterrupt()
        ioloop.IOLoop.instance().start()



if __name__ == "__main__":
    retval = Server().main(sys.argv)
    sys.exit(retval)
