import logging
import Ice, IceStorm
import server, views


Ice.loadSlice("-I /usr/share/Ice/slice /usr/share/duo/slice/DUO.ice --all")

import DUO

class ModelFactory:
    models = []
    cache = {}

    @classmethod
    def register(cls, model):
        cls.models.append(model)

    @classmethod
    def create(cls, proxy):
        if proxy in cls.cache:
            return cls.cache[proxy]

        for model_cls in cls.models:
            if model_cls.can_manage(proxy):
                model = model_cls(proxy)
                cls.cache[proxy] = model
                return model

class Model:
    def __init__(self):
        self.controller = None

    def setup(self, adapter):
        pass

    def set_controller(self, controller):
        self.controller = controller

    def subscribe_to_active(self, listener):
        proxy = DUO.Active.RPrx.uncheckedCast(self.proxy)
        observer = proxy.getObserver()
        topic = observer.ice_facet("topic")
        topic = IceStorm.TopicPrx.uncheckedCast(topic)
        topic.subscribeAndGetPublisher(None, listener)


class BoolModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.proxy = proxy

    def setup(self, adapter):
        listener = adapter.addWithUUID(IBoolListener(self.notify))
        self.subscribe_to_active(listener)

    def get(self):
        proxy=DUO.IBool.RPrx.checkedCast(self.proxy)
        return str(proxy.get()).lower()

    def set(self, value):
        proxy=DUO.IBool.WPrx.checkedCast(self.proxy)
        proxy.set(value == "true", Ice.Identity())

    def notify(self, value):
        self.controller.write_message(str(value).lower())

    @classmethod
    def can_manage(cls, proxy):
        print proxy
        typeids = proxy.ice_ids()
        return '::DUO::IBool::R' in typeids or '::DUO::IBool::W' in typeids


class IBoolListener(DUO.IBool.W):
    def __init__(self, cb):
        self.cb = cb

    def set(self, value, identity, current=None):
        self.cb(value)


class ByteModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.proxy = proxy

    def setup(self, adapter):
        listener = adapter.addWithUUID(IByteListener(self.notify))
        self.subscribe_to_active(listener)

    def get(self):
        proxy = DUO.IByte.RPrx.checkedCast(self.proxy)
        return str(proxy.get())

    def set(self, value):
        proxy=DUO.IByte.WPrx.checkedCast(self.proxy)
        proxy.set(value, Ice.Identity())

    def notify(self, value):
        self.controller.write_message(str(value))

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::DUO::IByte::R' in typeids or '::DUO::IByte::W' in typeids


class IByteListener(DUO.IByte.W):
    def __init__(self, cb):
        self.cb = cb

    def set(self, value, identity, current=None):
        self.cb(value)



ModelFactory.register(BoolModel)
ModelFactory.register(ByteModel)
