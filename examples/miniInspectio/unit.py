# -*- coding:utf-8; tab-width:4; mode:python -*-
import unittest

import Ice
Ice.loadSlice("-I /usr/share/Ice/slice /usr/share/duo/slice/DUO.ice --all")
import DUO
import models


class BoolR_I(DUO.IBool.R):
    def get(self, current):
        return True


class Test_BoolModel(unittest.TestCase):
    def setUp(self):
        self.ic = Ice.initialize()
        adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.dummy = adapter.addWithUUID(BoolR_I())
        adapter.activate()

    def test_can_manage(self):
        self.assertTrue(models.BoolModel.can_manage(self.dummy))
