#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

from tornado.websocket import WebSocketHandler
import tornado.ioloop
import tornado.web


class EchoWebSocket(WebSocketHandler):
    def __init__(self, *args):
        WebSocketHandler.__init__(self, *args)
        print "HOOLA"

    def open(self):
        print "WebSocket opened"
        self.write_message("hello")

    def on_message(self, message):
        print "You said:", message

    def on_close(self):
        print "WebSocket closed"


app = tornado.web.Application([
        (r"/hello", EchoWebSocket),
])


if __name__ == "__main__":
    app.listen(9999)
    tornado.ioloop.IOLoop.instance().start()

