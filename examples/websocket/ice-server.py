#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("sample.ice")
import Example


class HelloI(Example.Hello):
    def sayHello(self, current=None):
        print "You said Hello!"

        
class Server(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        adapter = ic.createObjectAdapterWithEndpoints("Adapter", "tcp -p 9000")
        adapter.activate()

        srv = HelloI()
        oid = ic.stringToIdentity("Hello")
        prx = adapter.add(srv, oid)
        
        print "PROXY at: '{0}'".format(prx)

        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == "__main__":
    retval = Server().main(sys.argv)
    sys.exit(retval)
    
    
