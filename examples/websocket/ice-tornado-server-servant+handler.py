#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

from tornado import ioloop, web, websocket
import sys
import Ice

Ice.loadSlice("sample.ice")
import Example


class HelloWebSocketFactory:
    def __init__(self, servant):
        self.servant = servant

    def __call__(self, *args):
        return HelloWebSocket(self.servant, *args)


class HelloWebSocket(websocket.WebSocketHandler):
    def __init__(self, servant, *args):
        websocket.WebSocketHandler.__init__(self, *args)
        servant.attach(self.say)

    def say(self, msg):
        self.write_message(msg)


class HelloI(Example.Hello):
    def __init__(self):
        self.observers = []
        
    def attach(self, observer):
        self.observers.append(observer)

    def say(self, msg, current=None):
        for ob in self.observers:
            ob(msg)

        
class Server(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        adapter = ic.createObjectAdapterWithEndpoints("Adapter", "tcp -p 9000")
        adapter.activate()

        servant = HelloI()

        app = web.Application([(r"/hello", HelloWebSocketFactory(servant))])
        app.listen(9999)
        
        oid = ic.stringToIdentity("Hello")
        prx = adapter.add(servant, oid)
        
        print "PROXY at: '{0}'".format(prx)       

        self.shutdownOnInterrupt()
        ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    retval = Server().main(sys.argv)
    sys.exit(retval)
    
    
