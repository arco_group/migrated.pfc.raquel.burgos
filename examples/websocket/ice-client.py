#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("sample.ice")
import Example

       
class Client(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        if len(args) != 2:
            print "Usage: {0} <proxy>".format(args[0])
            return 1
        
        prx = ic.stringToProxy(args[1])
        prx = Example.HelloPrx.checkedCast(prx)
        
        prx.say("Hello")


if __name__ == "__main__":
    retval = Client().main(sys.argv)
    sys.exit(retval)
    
    
