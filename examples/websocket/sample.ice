// -*- mode: c++; coding: utf-8 -*-

module Example {
  
  interface Hello {
    void say(string msg);
  };
  
};
