
class AFactory:
    def __init__(self, msg):
        self.msg = msg

    def __call__(self):
        return A(self.msg)


class A:
    def __init__(self, value):
        print value


def tornado(handler):
    print handler()



tornado(AFactory("hola"))

