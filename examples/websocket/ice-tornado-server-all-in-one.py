#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

from tornado import ioloop, web, websocket
import sys
import Ice

Ice.loadSlice("sample.ice")
import Example


class HelloWebSocketFactory:
    def __init__(self, adapter, identity):
        self.adapter = adapter
        self.identity = identity

    def __call__(self, *args):
        servant = HelloWebSocket(*args)
        proxy = self.adapter.add(servant, self.identity)
        print proxy
        return servant


class HelloWebSocket(websocket.WebSocketHandler, Example.Hello):
    def say(self, msg, current=None):
        self.write_message(msg)

        
class Server(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        adapter = ic.createObjectAdapterWithEndpoints("Adapter", "tcp -p 9000")
        adapter.activate()

        oid = ic.stringToIdentity("Hello")
        app = web.Application([(r"/hello", 
                                HelloWebSocketFactory(adapter, oid))])
        app.listen(9999)

        self.shutdownOnInterrupt()
        ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    retval = Server().main(sys.argv)
    sys.exit(retval)
    
    
