En este ejemplo, el estado del dummie puede ser cambiando desde el navegador web. A su vez, un publicador externo (a falta de un dummie 'activo' que nos informe de sus cambios de estado) publica un mensaje ('change status') que simula un cambio de estado del dummie. El servidor web, suscrito a este canal, lee el mensaje y lo envía al cliente web, que modifica su checkbox.


