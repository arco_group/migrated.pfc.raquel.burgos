# -*- coding: utf-8 -*-

import sys, Ice, IceStorm
Ice.loadSlice("/usr/share/duo/slice/DUO/DUO.ice -I /usr/share/Ice/slice --all")
import DUO
import time

class Publisher (Ice.Application):
    def run (self, argv):
        obj = self.communicator().stringToProxy('IceStorm/TopicManager:tcp -p 10000')
        topic_mgr_prx = IceStorm.TopicManagerPrx.checkedCast(obj)

        try:
            topic = topic_mgr_prx.retrieve('HelloTopic')
        except IceStorm.NoSuchTopic:
            topic = topic_mgr_prx.create('HelloTopic')

        pub = topic.getPublisher()
        boolw = DUO.IBool.WPrx.uncheckedCast(pub)

        boolw.set(True, Ice.Identity())
        time.sleep(1)
        boolw.set(False, Ice.Identity())


Publisher().main(sys.argv)
