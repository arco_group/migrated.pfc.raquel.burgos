#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

from tornado import ioloop, web, websocket
import sys
import Ice, IceStorm
import Inspectio

Ice.loadSlice("/usr/share/duo/slice/DUO/DUO.ice -I /usr/share/Ice/slice --all")

import DUO


class HTTP_Request_Handler(web.RequestHandler):
    def get(self):
        self.render('views/index.html')

class ModelFactory(web.RequestHandler):
    def get(self):
        #self.write("Estamos trabajando en ello...")
        self.render('views/BoolView.html')

class ControllerFactory:
    def __init__(self, proxy):
        self.proxy = proxy

    def __call__(self, *args):
        return Websocket(self.proxy, *args)


class Websocket(websocket.WebSocketHandler):
    def __init__(self, proxy, *args):
        websocket.WebSocketHandler.__init__(self, *args)
        self.model = BoolModel(self, proxy)

    def on_message(self, msg):
        method, param = msg.split(':')
        if method == 'get':
            status = self.model.get_status()
            self.write_message(status)
        else:
            self.model.set_status(param)


def subscribe(subscriber, topic_name):
    prx = Server.ic.stringToProxy('IceStorm/TopicManager:tcp -p 10000')
    topic_mgr = IceStorm.TopicManagerPrx.checkedCast(prx)

    try:
        topic = topic_mgr.retrieve(topic_name)
    except IceStorm.NoSuchTopic:
        topic = topic_mgr.create(topic_name)

    topic.subscribeAndGetPublisher(None, subscriber)


class BoolModel:
    def __init__(self, controller, proxy):
        self.controller = controller
        self.proxy = proxy
        self.setup_listener()

    def setup_listener(self):
        servant = IBoolListener(self.notify)
        listener = Server.adapter.addWithUUID(servant)
        subscribe(listener, "HelloTopic")

        #active = DUO.Active.RPrx.checkedCast(self.proxy)
        #print active.getObserver()

        # self.proxy.get_topic().subscribeAndGetPublisher(listener)

    def get_status(self):
        proxy=DUO.IBool.RPrx.checkedCast(self.proxy)
        return str(proxy.get()).lower()

    def set_status(self, value):
        self.proxy.set(value == "true", Ice.Identity())

    def notify(self, value):
        self.controller.write_message(str(value).lower())

    @classmethod
    def can_manage(cls, prx):
        if prx.ice_isA('DUO.IBool.R') or prx.ice_isA('DUO.IBool.W'):
            return True
        return False

class IBoolListener(DUO.IBool.W):
    def __init__(self, cb):
        self.cb = cb

    def set(self, value, identity, current=None):
        self.cb(value)

class Server(Ice.Application):
    def run(self, args):
        Server.ic = ic = self.communicator()
        proxy = ic.stringToProxy(sys.argv[1])
        print proxy
        print proxy.ice_ids()
        boolw = DUO.IBool.WPrx.checkedCast(proxy)

        Server.adapter = ic.createObjectAdapterWithEndpoints('InspectioAdapter', 'default')
        Server.adapter.activate()

        app = web.Application([(r"/", HTTP_Request_Handler),
                (r"/inspect-dummyBool", ControllerFactory(boolw)),(r"/[a-zA-Z0-9]+", ModelFactory)])
        app.listen(8080)

        self.shutdownOnInterrupt()
        ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    retval = Server().main(sys.argv)
    sys.exit(retval)
