
import Ice, IceStorm

Ice.loadSlice("-I /usr/share/slice -I{0} --all {1} {2}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice",
                                                           "/usr/share/slice/duo/ASDF.ice"))

import DUO, ASD

import json
from commodity import pattern

def marshall(*args):
    msg = {"param": [str(x) for x in args],}
    response = json.dumps(msg)
    return response
#    return [str(x).lower() for x in args]

def unmarshall(msg):
    message = json.loads(msg)
    method = message["method"]
    param = message["param"]
    model = message["model"]
    return method, param, model

class ModelFactory:
    models = []
    cache = {}

    @classmethod
    def register(cls, model):
        cls.models.append(model)

    @classmethod
    def create(cls, proxy):
        if proxy in cls.cache:
            return cls.cache[proxy]

        models = []
        for model_cls in cls.models:
            try:
                if model_cls.can_manage(proxy):
                    models.append(model_cls(proxy))
            except Exception, e:
                return None

        cls.cache[proxy] = models
        return models

class Model(object):
    def __init__(self):
        self.listener = None
        self.observers = []

    def setup(self, adapter):
        pass

    def attach(self, controller):
        self.observers.append(controller)

    def remove_controller(self, controller):
        self.observers.remove(controller)

    def subscribe_to_active(self, listener):
       # assert self.active
        observer = self.active.getObserver()
        assert observer, "{0} has not observer".format(self)
        topic = observer.ice_facet("topic")
        topic = IceStorm.TopicPrx.uncheckedCast(topic)

        topic.subscribeAndGetPublisher(None, listener)

    def processing_message(self, ic, msg):
        method, param, model= unmarshall(msg)
        if method == 'get':
            self.notify(self.get())
        elif method == 'setObserver':
            self.set_observer(ic, param)
        elif method == 'getObserver':
            self.notify(self.get_observer())
        elif method == 'link':
            key = param.split(' ')[0]
            prx = ic.stringToProxy(str(param))
            self.notify(self.link(key, prx))
        elif method == 'subscribe2topic':
            self.subscribe2topic(ic.stringToProxy(str(param[0])), ic.stringToProxy(str(param[1])))
        else:
            self.set(param)

    def get(self):
        assert self.reader
        return str(self.reader.get()).lower()

    def set(self, value):
        assert self.writer
        self.writer.set(value, Ice.Identity())

    def notify(self, value):
        for ob in self.observers:
            ob.write_message(marshall(value))
        print 'Enviado: '+marshall(value)

    def get_properties(self):
        pass

    def get_observer(self):
        pass

    def list(self):
        pass

    def retrieveAll(self):
        pass

    def link(self):
        pass

class BoolModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.reader = DUO.IBool.RPrx.checkedCast(proxy)
        self.writer = DUO.IBool.WPrx.checkedCast(proxy)
        self.active = DUO.Active.RPrx.checkedCast(proxy)

    def setup(self, adapter):
        if self.listener is not None:
            return
        self.listener = adapter.addWithUUID(IBoolListener(self.notify))
        self.subscribe_to_active(self.listener)

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::DUO::IBool::R' in typeids or '::DUO::IBool::W' in typeids


class IBoolListener(DUO.IBool.W):
    def __init__(self, cb):
        self.cb = cb

    def set(self, value, identity, current=None):
        self.cb(str(value).lower())


class ByteModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.reader = DUO.IByte.RPrx.checkedCast(proxy)
        self.writer = DUO.IByte.WPrx.checkedCast(proxy)
        self.active = DUO.Active.RPrx.checkedCast(proxy)


    def setup(self, adapter):
        if self.listener is not None:
            return
        self.listener = adapter.addWithUUID(IByteListener(self.notify))
        self.subscribe_to_active(self.listener)

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::DUO::IByte::R' in typeids or '::DUO::IByte::W' in typeids

class IByteListener(DUO.IByte.W):
    def __init__(self, cb):
        self.cb = cb

    def set(self, value, identity, current=None):
        self.cb(value)

class ObjectModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.proxy = proxy

    def get_identity(self):
        return self.proxy.ice_getIdentity()

    def get_ids(self):
        return self.proxy.ice_ids()

    def ping(self):
        return self.proxy.ice_ping()

    def get_id(self):
        return self.proxy.ice_id()

    def get_endpoints(self):
        return self.proxy.ice_getEndpoints()

    def get_properties(self):
        return {'Identity' : self.get_identity().name,
                'TypeID' : self.get_id(),
                'IDs' : self.get_ids(),
                'Endpoints' : self.get_endpoints()}

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::Ice::Object' in typeids

class ActiveModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.reader = DUO.Active.RPrx.checkedCast(proxy)
        self.writer = DUO.Active.WPrx.checkedCast(proxy)

    def get_observer(self):
        assert self.reader
        return self.reader.getObserver()

    def set_observer(self,ic, observer):
        assert self.writer
        try:
            ob = ic.stringToProxy(str(observer))
            self.observer = DUO.Active.WPrx.checkedCast(ob)
            self.writer.setObserver(self.observer)
        except Exception,e:
            self.notify('error')

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::DUO::Active::R' in typeids or '::DUO::Active::W' in typeids

class ActiveListener(DUO.Active.W):
    def __init__(self, cb):
        self.cb = cb

    def set(self, value, identity, current=None):
        self.cb(value)

class ContainerModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.reader = DUO.Container.RPrx.checkedCast(proxy)
        self.writer = DUO.Container.WPrx.checkedCast(proxy)

    def setup(self, adapter):
        pass

    def link(self, key, prx):
        assert self.writer
        print key, prx
        self.writer.link(key, prx)
        print self.reader.list()

    def list(self):
        return self.reader.list()

    def unlink(self, key):
        self.writer.unlink(key)

    def destroy(self):
        self.writer.destroy()

    def create(self, key):
        return self.writer.create(key)

    def get_childs(self, prx):
        models = ModelFactory.create(prx)
        response = ''
        msg={}
        if self.can_manage(prx):
            childs = models[0].list()
            print childs
            for child in childs.keys():
                msg["data"] = child
                msg["attr"] = childs[child]
                msg["children"] = self.get_childs(childs[child])
        else:
            return {}

        return msg

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::DUO::Container::R' in typeids or '::DUO::Container::W' in typeids or '::DUO::Container::RW' in typeids

class TopicManagerModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.reader = IceStorm.TopicManagerPrx.checkedCast(proxy)

    def retrieveAll(self):
        return self.reader.retrieveAll()

    def retrieve(self, topic):
        return self.reader.retrieve(topic)

    def create(self, topic):
        self.reader.create(topic)

    def subscribe2topic(self, topic, proxy):
        topic = IceStorm.TopicPrx.uncheckedCast(topic)
        topic.subscribeAndGetPublisher({}, proxy)

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::IceStorm::TopicManager' in typeids

class AsdaModel(Model):
    def __init__(self, proxy):
        Model.__init__(self)
        self.topic = IceStorm.TopicPrx.uncheckedCast(proxy)

    def setup(self, adapter):
        if self.listener is not None:
            return
        # create servant and add to adapter
        servant = TopicListener(self.notify)
        self.listener = adapter.addWithUUID(servant)

        # subscriber servant proxy to topic
        self.topic.subscribeAndGetPublisher({}, self.listener)

    def getName(self):
        return self.topic.getName()

    def getPublisher(self):
        return self.topic.getPublisher()

    def destroy(self):
        self.topic.destroy()

    @classmethod
    def can_manage(cls, proxy):
        typeids = proxy.ice_ids()
        return '::IceStorm::Topic' in typeids

class TopicListener(ASD.Listener):
    def __init__(self, cb):
        self.cb = cb

    def adv(self, prx, current=None):
        self.cb(prx)

ModelFactory.register(ByteModel)
ModelFactory.register(BoolModel)
ModelFactory.register(ContainerModel)
ModelFactory.register(ActiveModel)
ModelFactory.register(TopicManagerModel)
ModelFactory.register(AsdaModel)
ModelFactory.register(ObjectModel)
