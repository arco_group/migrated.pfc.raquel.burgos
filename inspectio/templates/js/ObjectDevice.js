function change_model(modelSelected, nameDevice, suffix){
    //var modelSelected = $('#'+id+' option:selected').text();
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(suffix != ''){
	$('#'+nameDevice.substring(0,12)+suffix).load(href, {suffix: suffix});
    }else{
	window.open(href, "_self");
    }
}
