if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}
function createWebsocket(nameDevice, suffix, serverIP){
    window['ws'+nameDevice] = new WebSocket('ws://'+serverIP+':8090/inspect-element/?proxy='+$('#valueDevice'+nameDevice+suffix).attr('href')+'&model=ByteModel');
}

function set_value(nameDevice, suffix, serverIP){
    var value = $('#valueDevice'+nameDevice+suffix).val();
    //if (($('#valueDevice'+nameDevice+suffix).val().length == 0) || $('#valueDevice'+nameDevice+suffix).val()>255 || $('#valueDevice'+nameDevice+suffix).val()<255 ){
    if(isNaN(value) || (value.length == 0) || (value>255) || (value<0)){
    //alert($(this).parent().getElementsByClassName('value')[0].val());

    //if(isNan(val)){
	alert('El objeto sólo acepta números enteros entre 0 y 255');
	return false;
    }
    var message = {'method' : 'set', 'param' : $('#valueDevice'+nameDevice+suffix).val(),'model' :$('#listModels'+nameDevice+suffix).attr('model')};

//var message = {'method' : 'set', 'param' : val,'model' :$('#listModels'+nameDevice).attr('model')};

 if (typeof (window['ws'+nameDevice]) == 'undefined'){
     createWebsocket(nameDevice, suffix, serverIP);
	window['ws'+nameDevice].onopen = function(){
	    window['ws'+nameDevice].send(JSON.stringify(message));
	};
    }else{
	window['ws'+nameDevice].send(JSON.stringify(message));
    }

    //$('#log'+nameDevice).val($('#log'+nameDevice).val()+('\nEnviado un set('+$('#valueDevice'+nameDevice).val()+')'));
    $('.log'+nameDevice).val($('.log'+nameDevice).val()+('\nEnviado un set('+$('#valueDevice'+nameDevice+suffix).val()+')'));
  //$('.log'+nameDevice).val($('.log'+nameDevice).val()+('\nEnviado un set('+val+')'));

    window['ws'+nameDevice].onmessage = function (message) {
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	//$('#valueDevice'+nameDevice).val(data);
	$('.value'+nameDevice).val(data);
	$('.log'+nameDevice).val($('.log'+nameDevice).val()+('\nRecibido('+$('#valueDevice'+nameDevice+suffix).val()+')'));
    };

}

function refresh_value(nameDevice, suffix, serverIP) {
    var message = {'method' : 'get', 'param' : null,'model' :$('#listModels'+nameDevice+suffix).attr('model')};
 if (typeof (window['ws'+nameDevice]) == 'undefined'){
     createWebsocket(nameDevice,suffix, serverIP);
	window['ws'+nameDevice].onopen = function(){
	    window['ws'+nameDevice].send(JSON.stringify(message));
	};
    }else{
	window['ws'+nameDevice].send(JSON.stringify(message));
    }

    window['ws'+nameDevice].onmessage = function (message) {
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	//$('#valueDevice'+nameDevice).val(data);
	$('.value'+nameDevice).val(data);
	$('.log'+nameDevice).val($('.log'+nameDevice).val()+('\nRecibido('+$('#valueDevice'+nameDevice+suffix).val()+')'));
    };

}

function change_view(viewSelected, nameDevice, suffix){
    //var viewSelected = $('#listViews'+nameDevice+suffix+' option:selected').text();
    var viewActual = $('#listViews'+nameDevice+suffix).attr('view');
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (viewSelected == viewActual){
	return false;
    }
    var proxy = $('#valueDevice'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view='+viewSelected+'&model='+modelActual;
    /*if(typeof window['ws'+nameDevice] != 'undefined'){
	window['ws'+nameDevice].close();
    }*/
    if(suffix != ''){
	$('#'+nameDevice+suffix).load(href, {suffix: suffix});
    }else{
	window.open(href, "_self");
    }
}

function change_model(modelSelected, nameDevice, suffix){
    //var modelSelected = $('#'+id+' option:selected').text();
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    /*if(typeof window['ws'+nameDevice] != 'undefined'){
	window['ws'+nameDevice].close();
    }*/
    if(suffix != ''){
	$('#'+nameDevice+suffix).load(href, {suffix: suffix});
	//$('.dynamicPanel'+nameDevice+suffix).load(href, {suffix: suffix});
    }else{
	//$('.dynamicPanel'+nameDevice+suffix).load(href);
	window.open(href, "_self");
    }
}

/*function change_model(nameDevice){
    var modelSelected = $('#listModels'+nameDevice+' option:selected').text();
    var modelActual = $('#listModels'+nameDevice).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(typeof window['ws'+nameDevice] != 'undefined'){
	window['ws'+nameDevice].close();
    }

    $('.dynamicPanel'+nameDevice).load(href);
}*/
