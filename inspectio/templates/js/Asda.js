if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}

function createWebsocketTopic(nameTopic, suffix, serverIP){
    window['ws'+nameTopic] = new WebSocket('ws://'+serverIP+':8090/inspect-element/?proxy='+$('#topic'+nameTopic+suffix).attr('href')+'&model=AsdaModel');
}

function waitAnnouncements(nameTopic, suffix, serverIP){
    if (typeof (window['ws'+nameTopic]) == 'undefined'){
	createWebsocketTopic(nameTopic, suffix, serverIP);
    }
    window['ws'+nameTopic].onmessage = function (message) {
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	var liTemplate =  '<li class="ui-widget-content ui-selectee" href="#{href}"> #{href}</li>';
	var li = $(liTemplate.replace( /#\{href\}/g, data));
	//$('.log'+nameTopic).val($('.log'+nameTopic).val()+('\nNuevo anunciamiento: ('+data+')'));
	$('.selectelements').append(li);
    };
}

function loadDevice(suffix){
    var proxy = $("#elementList"+suffix+" .ui-selected").attr('href');
    if(typeof(proxy)=='undefined'){ return false;}
    if($('#introducedProxy').length != 0){
    	$('#introducedProxy').val(proxy);
    	$('#searchProxy').click();
    	$('#introducedProxy').val('');
    }else{
    	var url = '?proxy='+encodeURIComponent(proxy)+'&view=default&model=default';
    	window.open(url);
    }
}

function change_model(modelSelected, nameDevice, suffix){
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(suffix != ''){
	$('#'+nameDevice+suffix).load(href, {suffix: suffix});
	//$('.dynamicPanel'+nameDevice+suffix).load(href, {suffix: suffix});
    }else{
	window.open(href, "_self");
	//$('.dynamicPanel'+nameDevice+suffix).load(href);
    }
}
