
$(document).ready(function() {

    var tabTemplate = '<li><a href=#{href}>#{label}</a> <span class=ui-icon ui-icon-close role=presentation>Remove Tab</span></li>';

    var tabs = $('#tabs').tabs();
    tabs.find('.ui-tabs-nav').sortable();

    var tabs2 = $('#tabs2').tabs();
    tabs2.find('.ui-tabs-nav').sortable();

  function load_device(){
    var link = $('#listDevices').val();
    var nam = $('#listDevices option:selected').text();

    if(!$('#'+nam+'-1').length && !$('#'+nam+'-2').length ){
	  var li = $(tabTemplate.replace( /#\{href\}/g, '#' + nam +'-1' ).replace( /#\{label\}/g, nam ));
          var li2 = $(tabTemplate.replace( /#\{href\}/g, '#'+nam+'-2').replace( /#\{label\}/g, nam));
          $('#tabcontent p').hide();
          $('#tabcontent2 p').hide();

          tabs.find( '.ui-tabs-nav' ).append( li );
          $('#tabcontent').append('<p id=''+nam+'-1'></p></div>');

          tabs2.find( '.ui-tabs-nav' ).append( li2 );
          $('#tabcontent2').append('<p id=''+nam+'-2'></p></div>');

          $('#'+nam+'-1').load(link, {suffix:'-1'});
          $('#'+nam+'-2').load(link, {suffix:'-2'});

          tabs.tabs('refresh');
          tabs2.tabs('refresh');


    }
  }

    function search_proxy(){
        proxy = $('#introducedProxy').val();
        var link ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model=default';
        var nam = proxy.split(' ')[0];

        if($('#'+nam+'-1').length || $('#'+nam+'-2').length ){
          /*  var panelId = tabs.find( ".ui-tabs-active" ).attr("aria-controls");
            if(panelId != nam){

            }*/
            return;
        }else{
            var li = $(tabTemplate.replace( /#\{href\}/g, '#' + nam+'-1' ).replace( /#\{label\}/g, nam ) );
            var li2 = $(tabTemplate.replace( /#\{href\}/g, '#'+nam+'-2').replace( /#\{label\}/g, nam) );
            $('#tabcontent p').hide();
            $('#tabcontent2 p').hide();
            tabs.find( '.ui-tabs-nav' ).append( li );
            $('#tabcontent').append('<p id=''+nam+'-1'></p></div>');
            tabs2.find( '.ui-tabs-nav' ).append( li2 );
            $('#tabcontent2').append('<p id=''+nam+'-2'></p></div>');
            $('#'+nam+'-1').load(link, {suffix: '-1'});
            $('#'+nam+'-2').load(link, {suffix: '-2'});
            tabs.tabs('refresh');
            tabs2.tabs('refresh');
        }
    }

    tabs.delegate( 'span.ui-icon-close', 'click', function() {
      var panelId = $( this ).closest( 'li' ).remove().attr( 'aria-controls' );
      //objectId = panelId.split("-");
      $( '#tabcontent p ' + panelId ).remove();
      $('#'+panelId).remove();
      tabs.tabs( 'refresh' );
      //$("#"+ob2).closest("li").remove().attr("aria-controls");
      //$("#tabcontent2 p " + objectId[0]+"-2" ).remove();
      //$("#"+objectId[0]+"-2").remove();
      //tabs2.tabs( "refresh" );
    });

   /* tabs.bind( "keyup", function( event ) {
      if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
        var panelId = tabs.find( ".ui-tabs-active" ).remove().attr( "aria-controls" );

        $( "#tabcontent p " + panelId ).remove();
        //$("#"+panelId).remove();
        tabs.tabs( "refresh" );
        $( "#tabcontent2 p " + panelId+"-2" ).remove();
        $("#"+panelId+"-2").remove();
        tabs2.tabs( "refresh" );
      }
    });*/

    tabs2.delegate( 'span.ui-icon-close', 'click', function() {
      var panelId = $( this ).closest( 'li' ).remove().attr( 'aria-controls' );
      $( '#tabcontent2 p ' + panelId ).remove();
      $('#'+panelId).remove();
      tabs2.tabs( 'refresh' );
    });

 /*   tabs2.bind( "keyup", function( event ) {
      if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
        var panelId = tabs2.find( ".ui-tabs-active" ).remove().attr( "aria-controls" );
        $( "#tabcontent2 p " + panelId ).remove();
        //$("#"+panelId).remove();
        tabs2.tabs( "refresh" );
        $( "#tabcontent p " + panelId ).remove();
        //$("#"+panelId).remove();
        tabs.tabs( "refresh" );
      }
    });*/

  $( '#loadDevice' )
      .button()
      .click(function() {
        load_device();
       });

  $('#searchProxy')
    .button()
    .click(function(){
       search_proxy();
    });

});
