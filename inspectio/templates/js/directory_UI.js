  $(function() {

    var tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>";

    var tabs = $("#tabs").tabs({
      event: "mouseover"
    });
    tabs.find(".ui-tabs-nav").sortable();

    var tabs2 = $("#tabs2").tabs({
      event: "mouseover"
    });
    tabs2.find(".ui-tabs-nav").sortable();

  function load_device(){
    var link = $("#listDevices").val();
      var nam = $("#listDevices option:selected").text().substring(0,12);

    if(!$("#"+nam+"-1").length && !$("#"+nam+"-2").length ){
	  var li = $(tabTemplate.replace( /#\{href\}/g, "#" + nam +"-1" ).replace( /#\{label\}/g, nam ));
          var li2 = $(tabTemplate.replace( /#\{href\}/g, "#"+nam+"-2").replace( /#\{label\}/g, nam));
          $("#tabcontent-1 p:visible").hide();
          $("#tabcontent-2 p:visible").hide();

          tabs.find( ".ui-tabs-nav" ).append( li );
          $("#tabcontent-1").append('<div><p id="'+nam+'-1"></p></div>');

          tabs2.find( ".ui-tabs-nav" ).append( li2 );
          $("#tabcontent-2").append('<div><p id="'+nam+'-2"></p></div>');

          $("#"+nam+"-1").load(link, {suffix:'-1'});
          $("#"+nam+"-2").load(link, {suffix:'-2'});
          tabs.tabs("refresh");
          tabs2.tabs("refresh");

	//   $("#"+nam+"-1").css({display: "block"});
	// $(li).attr('aria-selected', true);
	//   $("#"+nam+"-2").css({display: "block"});
    }
  }

    function search_proxy(){
        proxy = $("#introducedProxy").val();
        var link ="/handler/?proxy="+encodeURI(proxy)+'&view=default&model=default';
        var nam = proxy.split(" ")[0];
	if (nam.indexOf("/")>=0){
	    nam = nam.split("/")[1];
	}
	nam = nam.substring(0,12);
        if($("#"+nam+"-1").length || $("#"+nam+"-2").length ){
            return;
        }else{
            var li = $(tabTemplate.replace( /#\{href\}/g, "#" + nam+"-1" ).replace( /#\{label\}/g, nam ) );
            var li2 = $(tabTemplate.replace( /#\{href\}/g, "#"+nam+"-2").replace( /#\{label\}/g, nam) );
            $("#tabcontent-1 p:visible").hide();
            $("#tabcontent-2 p:visible").hide();
            tabs.find( ".ui-tabs-nav" ).append( li );
            $("#tabcontent-1").append('<div><p id="'+nam+'-1"></p></div>');
            tabs2.find( ".ui-tabs-nav" ).append( li2 );
            $("#tabcontent-2").append('<div><p id="'+nam+'-2"></p></div>');
            $("#"+nam+"-1").load(link, {suffix: '-1'});
            $("#"+nam+"-2").load(link, {suffix: '-2'});
            tabs.tabs("refresh");
            tabs2.tabs("refresh");
        }
	    // $("#"+nam+"-1").css({display: "block"});

	    // $("#"+nam+"-2").css({display: "block"});
    }

    tabs.delegate( "span.ui-icon-close", "click", function() {
      var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
      $( "#tabcontent-1 p " + panelId ).remove();
      $("#"+panelId).remove();
      tabs.tabs( "refresh" );
    });

    tabs2.delegate( "span.ui-icon-close", "click", function() {
      var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
      $( "#tabcontent-2 p " + panelId ).remove();
      $("#"+panelId).remove();
      tabs2.tabs( "refresh" );
    });

      $( "#loadDevice" )
	  .button()
	  .click(function() {
              load_device();
	  });

      $("#searchProxy")
	  .button()
	  .click(function(){
	      search_proxy();
	  });

});
