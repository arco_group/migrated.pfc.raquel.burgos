if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}

function createWebsocketActive(nameDevice, suffix, serverIP){
    window['wsa'+nameDevice] = new WebSocket('ws://'+serverIP+':8090/inspect-element/?proxy='+$('#active'+nameDevice+suffix).attr('href')+'&model=ActiveModel');
}

function setObserver(nameDevice, suffix, serverIP){
    var message = {'method' : 'setObserver', 'param' : $('#active'+nameDevice+suffix).val() ,'model' :'ActiveModel'};
    if(typeof window['wsa'+nameDevice] == 'undefined'){
	createWebsocketActive(nameDevice, suffix, serverIP);
	window['wsa'+nameDevice].onopen = function(){
	    window['wsa'+nameDevice].send(JSON.stringify(message));
	};
    }else{
	window['wsa'+nameDevice].send(JSON.stringify(message));
    }
    window['wsa'+nameDevice].onmessage = function(message){
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	if (data == 'error'){
	    alert('El proxy introducido es incorrecto');
	    getObserver(nameDevice, suffix);
	}else{
	    $('#active'+nameDevice+suffix).val(data);
	}
    };
}

function getObserver(nameDevice, suffix, serverIP){
    var message = {'method' : 'getObserver', 'param' : null ,'model' :'ActiveModel'};
    if(typeof window['wsa'+nameDevice] == 'undefined'){
	createWebsocketActive(nameDevice, suffix, serverIP);
	window['wsa'+nameDevice].onopen = function(){
	    window['wsa'+nameDevice].send(JSON.stringify(message));
	};
    }else{
	window['wsa'+nameDevice].send(JSON.stringify(message));
    }

    window['wsa'+nameDevice].onmessage = function(message){
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	if (data == 'error'){
	    alert('El proxy introducido es incorrecto');
	    getObserver(nameDevice, suffix);
	}else{
	    $('#active'+nameDevice+suffix).val(data);
	}
    };
}

function change_model(modelSelected, nameDevice, suffix){
    //var modelSelected = $('#'+id+' option:selected').text();
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(suffix != ''){
	$('#'+nameDevice+suffix).load(href, {suffix: suffix});
    }else{
	window.open(href, "_self");
    }
}
