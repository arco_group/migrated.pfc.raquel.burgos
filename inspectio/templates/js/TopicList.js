$(document).ready(function() {
    $( ".selectopics" ).selectable();
});

if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}

function createWebsocketTM(serverIP, suffix){
    window['wstm'] = new WebSocket('ws://'+serverIP+':8090/inspect-element/?proxy='+$('#topicList'+suffix).attr('href')+'&model=TopicManagerModel');
}

function change_model(modelSelected, nameDevice, suffix){
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(suffix != ''){
	$('#'+nameDevice+suffix).load(href, {suffix: suffix});
	 $( "#topicList"+suffix ).prop("selectable");
    }else{
	window.open(href, "_self");
	 $( "#topicList").prop("selectable");
    }
}

function loadTopic(suffix){
    var topic = $("#topicList"+suffix+" .ui-selected").attr('href');

    if(typeof(topic)=='undefined'){ return false;}
    if($('#introducedProxy').length != 0){
    	$('#introducedProxy').val(topic);
    	$('#searchProxy').click();
    	$('#introducedProxy').val('');
    }else{
    	var url = '?proxy='+encodeURIComponent(topic)+'&view=default&model=default';
    	window.open(url);
    }
}

function subscribe2topic(topic, device, suffix, serverIP){
    var message = {'method' : 'subscribe2topic', 'param' : [topic, device] ,'model' :'TopicManagerModel'};
    if(typeof window['wstm'] == 'undefined'){
	createWebsocketTM(serverIP, suffix);
	window['wstm'].onopen = function(){
	    window['wstm'].send(JSON.stringify(message));
	};
    }else{
	window['wstm'].send(JSON.stringify(message));
    }

}
