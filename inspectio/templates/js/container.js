if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}

function createWebsocketContainer(nameContainer, suffix, serverIP){
    window['wsc'+nameContainer] = new WebSocket('ws://'+serverIP+':8090/inspect-element/?proxy='+$('#container'+nameContainer+suffix).attr('href')+'&model=ContainerModel');
}


function change_model(modelSelected, nameContainer, suffix){
    //var modelSelected = $('#listModels'+nameBool+' option:selected').text();
    var modelActual = $('#listModels'+nameContainer+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameContainer+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(suffix != ''){
	$('#'+nameContainer.substring(0,12)+suffix).load(href, {suffix: suffix});
    }else{
	window.open(href, "_self");
    }
}

function loadChildren(id){
    if($('#introducedProxy').length != 0){
	$('#introducedProxy').val($('#'+id).attr('href'));
	$('#searchProxy').click();
	$('#introducedProxy').val('');
    }else{
	var url = '?proxy='+encodeURIComponent($('#'+id).attr('href'))+'&view=default&model=default';
	window.open(url);
    }
}

function linkChild(nameContainer, suffix, serverIP, proxy){
    if(proxy == ''){
	return false;
    }
    var message = {'method' : 'link', 'param' : proxy ,'model' :'ContainerModel'};
    if(typeof window['wsc'+nameContainer] == 'undefined'){
	createWebsocketContainer(nameContainer, suffix, serverIP);
	window['wsc'+nameContainer].onopen = function(){
	    window['wsc'+nameContainer].send(JSON.stringify(message));
	};
    }else{
	window['wsc'+nameContainer].send(JSON.stringify(message));
    }

    window['wsc'+nameContainer].onmessage = function(message){
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	if (data == 'error'){
	    alert('Error añadiendo el hijo');
	}else{
	    var prx = $('#listModels'+nameContainer+suffix).attr('href');
	     var href ='/handler/?proxy='+encodeURIComponent(prx)+'&view=default&model=default';
	    if(suffix != ''){
		$('#'+nameContainer.substring(0,12)+'-1').load(href, {suffix : '-1'});
		$('#'+nameContainer.substring(0,12)+'-2').load(href, {suffix : '-2'});
	    }else{
		window.open(href, "_self");
	    }
	}
    };

}

/*function loadChildren(id){
    var xmlhttp;
    var url = 'container/?proxy='+$('#'+id).attr('href');
    if(window.XMLHttpRequest){
	//Codigo para IE7+, Safari, Opera, Chrome, Firefox...
	xmlhttp = new XMLHttpRequest();
    }else{ //codigo para IE5, IE6
	xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
    }
    xmlhttp.onreadystatechange = appendChilds;
    xmlhttp.open('GET',url,true);
    xmlhttp.send(null);//con GET se envia null

    function appendChilds(){
	if(xmlhttp.readyState == 4){
	    var jsonResponse = JSON.parse(xmlhttp.responseText);
	    for(var i in jsonResponse){
		href = jsonResponse[i].Proxy;
		name = jsonResponse[i].Name;
		childs = jsonResponse[i].Childs;
		$('#ul li '+id).append('<ul><li id = '+id+'_'+name+' name='+name+'href='+href+'><a>'+name+'</a></li><ul>');
		//alert(name+' '+ href+' '+ childs);
	    }
	}
    }
}*/
