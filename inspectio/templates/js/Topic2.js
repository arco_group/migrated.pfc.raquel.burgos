if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}

function createWebsocketTopic(nameTopic, suffix, serverIP){
    window['ws'+nameTopic] = new WebSocket('ws://'+serverIP+':8090/inspect-element/?proxy='+$('#dynamicPanel'+nameTopic+suffix).attr('href')+'&model=TopicModel');
    //alert('creado websocket: '+window['ws'+nameTopic]);
}

/*function waitAnnouncements(nameTopic, suffix){
    if (typeof (window['ws'+nameTopic]) == 'undefined'){
	createWebsocketTopic(nameTopic, suffix);
    }
    window['ws'+nameTopic].onmessage = function (message) {
	//alert('recibido algo');
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	var liTemplate =  '<li class="ui-widget-content ui-selectee" href="#{href}"> #{href}</li>';
	var li = $(liTemplate.replace( /#\{href\}/g, data));
	//$('.log'+nameTopic).val($('.log'+nameTopic).val()+('\nNuevo anunciamiento: ('+data+')'));
	$('.selectelements').append(li);
    };
}*/

function waitAnnouncements(nameTopic, suffix){
    if (typeof (window['ws'+nameTopic]) == 'undefined'){
	createWebsocketTopic(nameTopic, suffix, serverIP);
    }
    window['ws'+nameTopic].onmessage = function (message) {
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	$('.selectelements')
         .append($("<option class='ui-widget-content ui-selectee'></option>")
         .attr("href",data)
         .text(data));

    };
}

function loadDevice(suffix){
    var proxy = $("#elementList"+suffix+" :selected").attr('href');
    if(typeof(proxy)=='undefined'){ return false;}
    if($('#introducedProxy').length != 0){
    	$('#introducedProxy').val(proxy);
    	$('#searchProxy').click();
    	$('#introducedProxy').val('');
    }else{
    	var url = '?proxy='+encodeURIComponent(proxy)+'&view=default&model=default';
    	window.open(url);
    }
}

function change_model(modelSelected, nameDevice, suffix){
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(suffix != ''){
	$('.dynamicPanel'+nameDevice+suffix).load(href, {suffix: suffix});
    }else{
	$('.dynamicPanel'+nameDevice+suffix).load(href);
    }
}
