$(document).ready(function() {
    $( ".selectopics" ).selectable();
});
function change_model(modelSelected, nameDevice, suffix){
    var modelActual = $('#listModels'+nameDevice+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameDevice+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
    if(suffix != ''){
	$('.dynamicPanel'+nameDevice+suffix).load(href, {suffix: suffix});
	 $( "#topicList"+suffix ).prop("selectable");
    }else{
	$('.dynamicPanel'+nameDevice+suffix).load(href);
	 $( "#topicList").prop("selectable");
    }
}

function loadTopic(suffix){
    var topic = $("#topicList"+suffix+" :selected").attr('href');
    if(typeof(topic)=='undefined'){ return false;}
    if($('#introducedProxy').length != 0){
    	$('#introducedProxy').val(topic);
    	$('#searchProxy').click();
    	$('#introducedProxy').val('');
    }else{
    	var url = '?proxy='+encodeURIComponent(topic)+'&view=default&model=default';
    	window.open(url);
    }
}
