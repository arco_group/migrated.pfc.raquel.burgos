if (! 'WebSocket' in window) {
    alert('Tu navegador no soporta Websockets');
    return;
}

function createWebsocket(nameBool, suffix, serverIP){
    window['ws'+nameBool] = new WebSocket('ws://'+serverIP+':8090/inspect-element/?proxy='+$('#stateDevice'+nameBool+suffix).attr('href')+'&model=BoolModel');
}

function set_state(nameBool, suffix, serverIP){
    var val = $('#stateDevice'+nameBool+suffix).is(':checked');
    var message = {'method' : 'set', 'param' : $('#stateDevice'+nameBool+suffix).is(':checked'),'model' :$('#listModels'+nameBool+suffix).attr('model')};
    $('.state'+nameBool).prop('checked', val);
    if (typeof (window['ws'+nameBool]) == 'undefined'){
	createWebsocket(nameBool, suffix, serverIP);
	window['ws'+nameBool].onopen = function(){
	    window['ws'+nameBool].send(JSON.stringify(message));
	};
    }else{
	window['ws'+nameBool].send(JSON.stringify(message));
    }
    $('.log'+nameBool).val($('.log'+nameBool).val()+('\nEnviado un set('+$('#stateDevice'+nameBool+suffix).is(':checked')+')'));
    window['ws'+nameBool].onmessage = function (message) {
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	$('.state'+nameBool).prop('checked', data=='true');

	    act_view();

	$('.log'+nameBool).val($('.log'+nameBool).val()+('\nRecibido('+data+')'));
    };

}

function refresh_state(nameBool, suffix, serverIP) {

    var message = {'method' : 'get', 'param' : null,'model' :$('#listModels'+nameBool+suffix).attr('model')};

    if (typeof (window['ws'+nameBool]) == 'undefined'){
	createWebsocket(nameBool, suffix, serverIP);
	window['ws'+nameBool].onopen = function(){
	    window['ws'+nameBool].send(JSON.stringify(message));
	};
    }else{
	window['ws'+nameBool].send(JSON.stringify(message));
    }

    window['ws'+nameBool].onmessage = function (message) {
	var response = JSON.stringify(message.data);
	var data = (JSON.parse(message.data)).param;
	$('.state'+nameBool).prop('checked', data=='true');

	    act_view();

	$('.log'+nameBool).val($('.log'+nameBool).val()+('\nRecibido('+data+')'));
    };
}

function change_view(viewSelected, nameBool, suffix){
    var viewActual = $('#listViews'+nameBool+suffix).attr('view');
    var modelActual = $('#listModels'+nameBool+suffix).attr('model');
    if (viewSelected == viewActual){
	return;
    }
    var proxy = $('#stateDevice'+nameBool+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view='+viewSelected+'&model='+modelActual;
    if(suffix != ''){
	//$('.dynamicPanel'+nameBool+suffix).load(href, {suffix: suffix});
	$('#'+nameBool+suffix).load(href, {suffix: suffix});
    }else{
	//$().load(href);
	window.open(href, "_self");
	//$('.dynamicPanel'+nameBool+suffix).load(href);
	//$(this).parent().load(href);
    }
}

function change_model(modelSelected, nameBool, suffix){
    //var modelSelected = $('#listModels'+nameBool+' option:selected').text();
    var modelActual = $('#listModels'+nameBool+suffix).attr('model');
    if (modelSelected == modelActual){
	return false;
    }
    var proxy = $('#listModels'+nameBool+suffix).attr('href');
    var href ='/handler/?proxy='+encodeURIComponent(proxy)+'&view=default&model='+modelSelected;
/*    if(typeof window['ws'+nameBool] != 'undefined'){
	window['ws'+nameBool].close();
    }*/
    if(suffix != ''){
	//$('.dynamicPanel'+nameBool+suffix).load(href, {suffix: suffix});
	$('#'+nameBool+suffix).load(href, {suffix: suffix});
    }else{
	//$('.dynamicPanel'+nameBool+suffix).load(href);
	window.open(href, "_self");
    }
}

function act_view(){
    if ( $('.state{{escape(name)}}').is(':checked')){
      //alert('llega true');

     var parent = $(".cb-enable").parents('.switch');
      $('.cb-disable',parent).removeClass('selected');
      $('.cb-enable',parent).addClass('selected');
    }else{
      var parent = $(".cb-disable").parents('.switch');
      $('.cb-enable',parent).removeClass('selected');
      $('.cb-disable',parent).addClass('selected');
    }
}
