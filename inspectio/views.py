# -*- coding:utf-8; tab-width:4; mode:python -*-
import os

class ViewFactory:
    views = {}
    @classmethod
    def register(cls, model_name, view):
        cls.views[model_name] = view

    @classmethod
    def create(cls, model):
        return cls.views[model.__class__.__name__]

class View:
    def get_template(self):
        return self.templates[self.template_by_default]

    def get_name_by_default(self):
        return self.template_by_default

    def get_list_templates(self):
        return self.templates

    def get_selected_template(self, nameView):
        if nameView == 'default':
            return self.get_template(), self.get_name_by_default()
            #include = '{% include ' + self.get_template()+' %}'
        return self.templates[nameView], nameView

    def get_script(self):
        s = open(self.script, 'r')
        script= s.read()
        s.close()
        return self.script

class BoolView(View):
    def __init__(self):
        self.templates = {"Checkbox":"templates/checkbox.html",
                           "Image":"templates/image_checkbox.html"}
        self.template_by_default = "Checkbox"
        self.script = "inspectio/templates/js/BooleanDevice.js"

class ByteView(View):
    def __init__(self):
        self.templates = {"Textbox" : "templates/textbox.html",
                          "Spinner": "templates/spinner.html"}
        self.template_by_default = "Textbox"
        self.script = "inspectio/templates/js/ByteDevice.js"

class ObjectView(View):
    def __init__(self):
        self.templates = {"Object" : "templates/object.html"}
        self.template_by_default = "Object"
        self.script = "inspectio/templates/js/ObjectDevice.js"

class ActiveView(View):
    def __init__(self):
        self.templates = {"Active" : "templates/active.html"}
        self.template_by_default = "Active"
        self.script = "inspectio/templates/js/ActiveDevice.js"

class ContainerView(View):
    def __init__(self):
        self.templates = {"Container" : "templates/container.html"}
        self.template_by_default = "Container"
        self.script = "inspectio/templates/js/container.js"

class TopicManagerView(View):
    def __init__(self):
        self.templates = {"TopicManager" : "templates/topic_list.html"}
        self.template_by_default = "TopicManager"
        self.script = "inspectio/templates/js/TopicList.js"

class AsdaView(View):
    def __init__(self):
        self.templates = {"Announcements" : "templates/asda.html"}
        self.template_by_default = "Announcements"
        self.script = "inspectio/templates/js/Asda.js"

ViewFactory.register('BoolModel', BoolView())
ViewFactory.register('ByteModel', ByteView())
ViewFactory.register('ObjectModel', ObjectView())
ViewFactory.register('ActiveModel', ActiveView())
ViewFactory.register('ContainerModel', ContainerView())
ViewFactory.register('TopicManagerModel', TopicManagerView())
ViewFactory.register('AsdaModel', AsdaView())
