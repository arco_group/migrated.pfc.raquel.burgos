#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import sys, os
import logging
logging.getLogger().setLevel(logging.DEBUG)
import codecs
import Ice, IceStorm
from tornado import ioloop, web, websocket, template
import json
import models, views, controllers
import socket

Ice.loadSlice("-I /usr/share/slice -I{0} --all {1}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice"))
import DUO

dummies = {"Hello1": "Hello1 -t:tcp -h 127.0.0.1 -p 8889",
           #"Hello2": "Hello2 -t:tcp -h 127.0.0.1 -p 8890",
           "Byte1": "Byte1 -t:tcp -h 127.0.0.1 -p 8892",
           "TopicManager" : "ISTopicAsFacet/TopicManager -t:tcp -h 127.0.0.1 -p 11000",
           "ASDA": "ASDA -t:tcp -h 161.67.106.59 -p 11000" }

def marshall(*args):
    msg = {"params": [str(x) for x in args],}
    response = json.dumps(msg)
    return response

def toListModels(models):
    retval = []
    for item in models:
        retval.append(item.__class__.__name__)
    return retval

def get_childs(prx):

    mds = []
    mds = models.ModelFactory.create(prx)

    if models.ContainerModel.can_manage(prx):
        childs = mds[0].list()
        msg = {}
        for child in childs.keys():
            msg.update({child : {"Name": child, "Proxy" : str(childs[child]), "Childs" :
                        get_childs(childs[child])}})
    else:
        return {}
    return msg


class ObjectDirectoryHandler(web.RequestHandler):
    def get(self):
        self.render('templates/directory_UI.html', availableDevices=dummies)


class ObjectHandler(web.RequestHandler):
    def get(self):
        try:
            proxy = self.request.arguments.get('proxy')[0]
            viewSelected = self.request.arguments.get('view')[0]
            model = self.request.arguments.get('model')[0]

            self.processRequest(proxy, viewSelected, model, '')

        except TypeError, e:
            print e
            #TODO retornar al navegador mensaje de error
            self.write("Ruta incorrecta, comprueba los argumentos")
            return

        except Exception, e:
            print e
            self.write("Error: " + str(e))
            return

    def post(self):
        try:
            proxy = self.request.arguments.get('proxy')[0]
            viewSelected = self.request.arguments.get('view')[0]
            model = self.request.arguments.get('model')[0]
            suffix = self.request.arguments.get('suffix')[0]

            self.processRequest(proxy, viewSelected, model, suffix)

        except TypeError, e:
            print e
            #TODO retornar al navegador mensaje de error
            self.write("Ruta incorrecta, comprueba los argumentos")
            return

        except Exception, e:
            print e
            self.write_error(str(e))
            return

    def processRequest(self, proxy, viewSelected, model, suffix):
        try:
            self.models = []
            self.proxy = Server.ic.stringToProxy(proxy)
            self.models = models.ModelFactory.create(self.proxy)

            if self.models:

                self.listModels = toListModels(self.models)

                indexModel = 0
                if model != 'default':
                    indexModel = self.listModels.index(model)

                self.models[indexModel].setup(Server.adapter)

                self.view = views.ViewFactory.create(self.models[indexModel])


                self.pag, self.viewName = self.view.get_selected_template(viewSelected)

                self.listViews = self.view.get_list_templates()

                self.name = str(self.proxy.ice_getIdentity().name)

                self.properties = self.models[indexModel].get_properties()

                self.observer = self.models[indexModel].get_observer()

                self.childs = self.models[indexModel].list()

                self.topics = self.models[indexModel].retrieveAll()

                self.render(self.pag, name=self.name, href=self.proxy, topics = self.topics,
                         model=str(self.models[indexModel].__class__.__name__),
                         models=self.listModels, views=self.listViews, suffix=suffix,
                         view=self.viewName, properties=self.properties, observer=self.observer,
                         childs = self.childs, serverIP = Server.ip)
            else:
            #enviar al navegador mensaje de error
                self.write("Dispositivo no encontrado")
             #self.write(alert("Problemas"))
        except Exception, e:
            self.write("Error creando el modelo: " + str(e))

class Server(Ice.Application, web.Application):
    def run(self, args):
        argv = ['--Ice.Config=config/local.config']
       # Server.ic = ic = self.communicator()
        Server.ic = ic = Ice.initialize(argv)
        Server.adapter = ic.createObjectAdapterWithEndpoints('InspectioAdapter', 'default')
        Server.adapter.activate()

        app = web.Application([
                (r"/", ObjectDirectoryHandler),
                (r"/inspect-element/", controllers.ControllerFactory(Server.ic)),
                (r"/handler/", ObjectHandler)],
                #(r"/container/", ContainerHandler),
                static_path = os.path.join(os.path.dirname(__file__), "templates"),
             )

        app.listen(8090)
        Server.ip = socket.gethostbyaddr(socket.gethostname())[2][0]
        print "Servidor escuchando en el puerto 8090..."
        self.shutdownOnInterrupt()
        ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    retval = Server().main(sys.argv)
    sys.exit(retval)
