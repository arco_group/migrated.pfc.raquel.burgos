import logging
from tornado import ioloop, web, websocket
import models
import json

def unmarshall(msg):
    message = json.loads(msg)
    method = message["method"]
    param = message["param"]
    model = message["model"]
    return method, param, model

def toListModels(mdls):
    models = []
    for item in mdls:
        models.append(item.__class__.__name__)
    return models


class ControllerFactory:
    def __init__(self, ic):
        self.ic = ic

    def create(self, *args):
        return Websocket(self.ic, *args)
#        ws.append(Websocket(self.ic, *args))

    def __call__(self, *args):
        return self.create(*args)


class Websocket(websocket.WebSocketHandler):
    def __init__(self, ic, *args):
        self.ic = ic
        websocket.WebSocketHandler.__init__(self,*args)
        print args
        path = self.request.arguments.get('proxy')[0]
        model = self.request.arguments.get('model')[0]
        proxy = ic.stringToProxy(path)
        self.models = models.ModelFactory.create(proxy)
        self.listModels = toListModels(self.models)
        indexModel = 0
        if model != 'default':
            indexModel = self.listModels.index(str(model))
        #for model in self.models:
        self.models[indexModel].attach(self)

    def __call__(self, ic, *args):
        return self

    def on_message(self, msg):
        print ('Recibido: ', msg)
        method, param, model= unmarshall(msg)

        self.listModels = toListModels(self.models)
        indexModel = 0
        if model != 'default':
            indexModel = self.listModels.index(str(model))
        self.models[indexModel].processing_message(self.ic, msg)

    def on_close(self):
        for model in self.models:
            model.remove_controller(self)

#        method, params = algo.unmarshal(self.model, msg)
#        method, param = msg.split(':')
#        if method == 'get':
#            status = self.model.get()
#            self.write_message(status.lower())
#        else:
#            if self.model.__class__.__name__ == "ByteModel":
#                self.model.set(param)
#            else:
#                self.model.set(param.capitalize()=="True")
