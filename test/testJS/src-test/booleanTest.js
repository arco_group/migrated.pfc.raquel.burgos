QueueTest = AsyncTestCase("QueueTest");

QueueTest.prototype.setUp = function() {
	
	$("body").append("<input class='checkbox stateHello1' type='checkbox' id='stateDevice'" +
			"<button class='button' id='upButton' onClick=refresh_state()>Actualizar</button> ");
};

QueueTest.prototype.testGetTrue = function(queue) {
	  queue.call('Paso 1: hacer la llamada al refresh', function() {
		  $('#upButton').click();
	  });

	  queue.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { });
		  window.setTimeout(myCallback, 5000);
	  });

	  queue.call('Paso 3: comprobar que el valor es true', function() {
		  jstestdriver.console.log("ahora tiene: "+$("#stateDeviceHello1").is(":checked"));
		  assertTrue($('#stateDeviceHello1').is(':checked'));
	  });
};

QueueTest.prototype.testSetFalse = function(queue2) {
	  queue2.call('Paso 1: hacer la llamada al set', function() {
		  $('#setButton').click();
		  jstestdriver.console.log("ha hecho click y ahora tiene: "+$("#stateDeviceHello1").is(":checked"));
	  });

	  queue2.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { 
		  });
		  window.setTimeout(myCallback, 1000);
	  });

	  queue2.call('Paso 3: comprobar que el valor es false', function() {
		  assertFalse($('#stateDevice').is(':checked'));
	  });
};