ActiveTest = AsyncTestCase("ActiveTest");

ActiveTest.prototype.setUp = function() {
	
	$("body").append("<input class='textbox' size='80' type='textbox' id='activeHello1'" +
			" name='Hello1' href='Hello1 -t:tcp -h 127.0.0.1 -p 8889' placeholder=''/>" +
			" <button class='button' id='setButtonHello1' name='Hello1' onClick=setObserver(name,'','localhost')>Asignar</button>"+
			" <button class='button' id='getButtonHello1' name='Hello1' onClick=getObserver(name,'','localhost')></button>");
};

ActiveTest.prototype.testSetObserverBool = function(queue) {
	queue.call('Paso 1: hacer la llamada al set', function() {
		$('#activeHello1').val('Hello2 -t:tcp -h 127.0.0.1 -p 8899');
		$('#setButtonHello1').click();
		//$('#activeHello1').val('');
	});

	queue.call('Paso 2: esperar un segundo', function(callbacks) {
		var myCallback = callbacks.add(function() {
		});
		window.setTimeout(myCallback, 1000);
	});
	
	queue.call('Paso 3: comprobar que el valor del observador es el correcto', function() {
		  $('#getButtonHello1').click();
	});
	
	queue.call('Paso4', function(callbacks){
		var myCallback2 = callbacks.add(function() {
		});
		window.setTimeout(myCallback2, 2000);
	});
	
	queue.call('Paso : comprobar que el valor del observador es el correcto', function() {
		  assertEquals($('#activeHello1').val(), 'Hello2 -t:tcp -h 127.0.0.1 -p 8899');
	});
};
	

ActiveTest.prototype.testGetObserverBool = function(queue) {
	queue.call('Paso 1: hacer la llamada al set', function() {
		$('#getButtonHello1').click();
	});

	queue.call('Paso 2: esperar un segundo', function(callbacks) {
		var myCallback = callbacks.add(function() {
		});
		window.setTimeout(myCallback, 1000);
	});
	
	queue.call('Paso : comprobar que el valor del observador es el correcto', function() {
		  assertEquals($('#activeHello1').val(), 'Hello2 -t:tcp -h 127.0.0.1 -p 8899');
	});
};