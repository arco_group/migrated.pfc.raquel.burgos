ByteTest = AsyncTestCase("ByteTest");

ByteTest.prototype.setUp = function() {
	
	$("body").append("<input class='textbox valueByte1' type='textbox' id='valueDeviceByte1' name='Byte1'" +
			"href='Byte1 -t:tcp -h 127.0.0.1 -p 8892'></input>" +
			"<select name='Byte1' id='listModelsByte1' model='ByteModel'></select>" +
			"<button class='button' id='validateButtonByte1' name='Byte1' onClick=set_value(name,'','localhost')>Validar</button>" +
			"<button class='button' id='updateButtonByte1' name='Byte1' onClick=refresh_value(name,'','localhost')>Actualizar</button>");
};

ByteTest.prototype.testGet15 = function(queue) {
	  queue.call('Paso 1: hacer la llamada al refresh', function() {
		  $("#updateButtonByte1").click();
	  });

	  queue.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { });
		  window.setTimeout(myCallback, 1000);
	  });

	  queue.call('Paso 3: comprobar que el valor es 15', function() {
		  jstestdriver.console.log("ahora tiene: "+$("#valueDeviceByte1").val());
		  assertEquals($('.valueByte1').val(), 15);
	  });
};

ByteTest.prototype.testSet45 = function(queue) {
	  queue.call('Paso 1: hacer la llamada al set', function() {
		  $('#valueDeviceByte1').val(45);
		  $("#validateButtonByte").click();
	  });

	  queue.call('Paso 2: esperar un segundo', function(callbacks) {
		  var myCallback = callbacks.add(function() { });
		  window.setTimeout(myCallback, 1000);
	  });
	  
	  queue.call('Paso 3: comprobar que el valor es 15', function() {
		  jstestdriver.console.log("ahora tiene: "+$("#valueDeviceByte1").val());
		  assertEquals($('#valueDeviceByte1').val(), 45);
	  });
};

