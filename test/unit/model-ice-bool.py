# -*- coding:utf-8; tab-width:4; mode:python -*-

import time

import sys
sys.path.append('test')
sys.path.insert(0, '.')
import threading

from unittest import TestCase
from mock import Mock
from commodity.os_ import SubProcess
from commodity.testing import wait_that, listen_port, localhost, assert_that
import json

import Ice, IceStorm
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice"))
import DUO
Ice.loadSlice("-I/usr/share/slice/duo -I{0} --all {1}".format(Ice.getSliceDir(),
                                                               "/usr/share/slice/duo/dummy/dummy.ice"))
import DummyDUO


from inspectio.models import BoolModel, IBoolListener

def unmarshall(msg):
    message = json.loads(msg)
    param = message["param"]
    return param

def get_topic(ic, name):
    topic_manager = ic.propertyToProxy('IceStorm.TopicManager.Proxy')
    topic_manager_prx = IceStorm.TopicManagerPrx.checkedCast(topic_manager)
    try:
        return topic_manager_prx.retrieve(name)
    except IceStorm.NoSuchTopic:
        return topic_manager_prx.create(name)


class BoolRWA_I(DummyDUO.IBool.RWA):
    def __init__(self):
        self.value = False

    def get(self, current):
        return self.value

    def set(self, value, oid, current):
        self.value = value

    def getObserver(self, current):
        return None


class Controller(object):
    pass


class TestBoolModel(TestCase):
    def setUp(self):
        argv = ['--Ice.Config=config/local.config']

        self.istaf = SubProcess("duo-istaf")

        wait_that(localhost, listen_port(10000))

        self.ic = Ice.initialize(argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.servant = BoolRWA_I()
        self.dummy = self.adapter.addWithUUID(self.servant)
        self.adapter.activate()

        self.sut = BoolModel(self.dummy)

        self.addCleanup(self.istaf.terminate)

    def create_observer_topic(self):
        topic = get_topic(self.ic, 'bool-topic')
        #publisher = topic.getPublisher()
        self.servant.getObserver = Mock()
        self.servant.getObserver.return_value = topic.getPublisher()
        return topic

    def add_listener(self):
        notify = Mock()
        listener = self.adapter.addWithUUID(IBoolListener(notify))
        self.sut.subscribe_to_active(listener)
        return notify

    def test_can_manage(self):
        self.assertTrue(BoolModel.can_manage(self.dummy))

    def test_get_initial_value_is_false(self):
        self.assertEquals(self.sut.get(), 'false')

    def test_set(self):
        self.sut.set(True)
        self.assertEquals(self.sut.get(), 'true')

    def test_model_create_listener_for_actives(self):
        self.create_observer_topic()
        self.add_listener()

        self.assert_(self.servant.getObserver.called)

    def test_listener_is_invoked_by_active(self):
        topic = self.create_observer_topic()
        notify = self.add_listener()

        boolw = DUO.IBool.WPrx.uncheckedCast(topic.getPublisher())
        boolw.set(True, Ice.Identity())
        time.sleep(2)
        self.assert_(notify.called)

    def test_listener_is_a_singleton(self):
        self.create_observer_topic()
        self.sut.setup(self.adapter)
        listener1 = self.sut.listener

        self.sut.setup(self.adapter)
        listener2 = self.sut.listener

        self.assertEquals(listener1, listener2)

    def test_controller_invoked(self):
        event = threading.Event()

        topic = self.create_observer_topic()
        self.sut.setup(self.adapter)

        controller = Controller()
        controller.write_message = Mock()
        controller.write_message.side_effect = lambda x: event.set()
        self.sut.attach(controller)

        boolw = DUO.IBool.WPrx.uncheckedCast(topic.getPublisher())
        boolw.set(True, Ice.Identity())

        event.wait(1)

        self.assert_(controller.write_message.called)
        response = unmarshall(controller.write_message.call_args[0][0])
        self.assertEquals(response[0], 'true')
