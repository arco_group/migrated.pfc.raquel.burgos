# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.append('test')
sys.path.insert(0, '.')
import threading
import time

from unittest import TestCase
from hamcrest import is_not
from commodity.os_ import SubProcess, check_output
from commodity.testing import wait_that, listen_port, localhost, assert_that
from mock import Mock

import json

import Ice, IceStorm
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1} {2}".format(
        Ice.getSliceDir(),
        "/usr/share/slice/duo/DUO.ice",
        "inspectio/ContextService/ContextService.ice"))


import DUO, ContextService

Ice.loadSlice("-I/usr/share/slice/duo -I{0} --all {1}".format(Ice.getSliceDir(),
                                                               "/usr/share/slice/duo/dummy/dummy.ice"))
import DummyDUO

from inspectio.models import ContainerModel, ByteModel, IByteListener

env = {'LD_LIBRARY_PATH': 'src/'}

class ByteRWA_I(DummyDUO.IByte.RWA):
    def __init__(self):
        self.value = 0

    def get(self, current):
        return self.value

    def set(self, value, oid, current):
        self.value = value

class TestContainerModel(TestCase):
    def setUp(self):
        check_output('mkdir -p /tmp/db')
        check_output('mkdir -p /tmp/psdb')
        check_output('mkdir -p /tmp/csdb')

        argv = ['--Ice.Config=config/local.config']

        #self.icestorm = SubProcess(
        #    "icebox --Ice.Config=config/icebox.IS.config")

        self.contextserver = SubProcess(
            'icebox --Ice.Config=config/icebox-example.cfg', env=env)

        self.istaf = SubProcess("duo-istaf")

        wait_that(localhost, listen_port(10000))
        wait_that(localhost, listen_port(20002))

        self.ic = Ice.initialize(argv)

        self.base = self.ic.propertyToProxy("ContextServer.Proxy")

        # Retrieve a proxy
        self.dummy = ContextService.ContainerPrx.checkedCast(self.base)
        self.container = ContainerModel(self.dummy)

        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.servant = ByteRWA_I()
        self.dummyByte = self.adapter.addWithUUID(self.servant)
        self.adapter.activate()
        self.sutByte = ByteModel(self.dummy)

        self.addCleanup(self.contextserver.terminate)
        #self.addCleanup(self.icestorm.terminate)
        self.addCleanup(self.istaf.terminate)

    def tearDown(self):
        for ch in self.container.list().keys():
            self.container.unlink(ch)
        self.container.destroy()

    def test_list(self):
        self.container.create('Child1')
        child2 = self.container.create('Child2')
        child2.link('Byte', self.dummyByte)
        print self.container.list()
