from unittest import TestCase
import Ice, IceStorm
from commodity.os_ import SubProcess
from commodity.testing import wait_that, listen_port, localhost, assert_that
from inspectio.models import TopicManagerModel

class BoolRWA_I(DummyDUO.IBool.RWA):
    def __init__(self):
        self.value = None

class TestTopicManagerModel(TestCase):
    def setUp(self):
        argv = ['--Ice.Config=config/local.config']

        self.istaf = SubProcess("duo-istaf")

        wait_that(localhost, listen_port(10000))


        key = "IceStorm.TopicManager.Proxy"
        self.ic = Ice.initialize(argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        proxyIceStorm = self.ic.propertyToProxy(key)
        self.topic_mng = IceStorm.TopicManagerPrx.checkedCast(proxyIceStorm)

        self.addCleanup(self.istaf.terminate)

    def test_set_and_get_topics(self):
        self.topic_mng.create('Test')
        self.assertEquals(len(self.topic_mng.retrieveAll()), 1)

    def test_get_topic(self):
        self.topic_mng.create('Test2')
        self.assertTrue(self.topic_mng.retrieve('Test2'))

    def test_can_manage(self):
        self.assertTrue(TopicManagerModel.can_manage(self.topic_mng))

    # def test_subscribe2topic(self):
    #     self.topic_mng.create('Topic')
    #     self.servant = BoolRWA_I()
    #     self.dummy = self.adapter.addWithUUID(self.servant)
    #     self.topic_mng.subscribe2topic(self.topic_mgn.retrieve('Topic'), self.dummy)
    #     self.assertEquals(self.topic_mgn.retrieve('Topic').getPublisher
