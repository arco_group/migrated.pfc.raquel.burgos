# -*- coding:utf-8; tab-width:4; mode:python -*-

import time

import sys
sys.path.append('test')
sys.path.insert(0, '.')
import threading

from unittest import TestCase
from mock import Mock

import json

import Ice, IceStorm
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice"))
import DUO
Ice.loadSlice("-I/usr/share/slice/duo -I{0} --all {1}".format(Ice.getSliceDir(),
                                                               "/usr/share/slice/duo/dummy/dummy.ice"))
import DummyDUO

from inspectio.models import ObjectModel

def unmarshall(msg):
    message = json.loads(msg)
    param = message["param"]
    return param

def get_topic(ic, name):
    topic_manager = ic.propertyToProxy('IceStorm.TopicManager.Proxy')
    topic_manager_prx = IceStorm.TopicManagerPrx.checkedCast(topic_manager)
    try:
        return topic_manager_prx.retrieve(name)
    except IceStorm.NoSuchTopic:
        return topic_manager_prx.create(name)

class BoolRWA_I(DummyDUO.IBool.RWA):
    def __init__(self):
        self.value = None

    def get(self, current):
        return self.value

    def set(self, value, oid, current):
        self.value = value

    def getObserver(self, current):
        return None

class Controller(object):
    pass


class TestObjectModel(TestCase):
    def setUp(self):
        argv = ['--Ice.Config=config/local.config']
        self.ic = Ice.initialize(argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.servant = BoolRWA_I()
        self.dummy = self.adapter.addWithUUID(self.servant)
        self.adapter.activate()

        self.sut = ObjectModel(self.dummy)

    def test_can_manage(self):
        self.assertTrue(ObjectModel.can_manage(self.dummy))

    def test_get_ids(self):
        self.assertEquals(self.sut.get_ids(), self.dummy.ice_ids())

    def test_get_id(self):
        self.assertEquals(self.sut.get_id(), self.dummy.ice_id())

    def test_get_endopoints(self):
        self.assertEquals(self.sut.get_endpoints(), self.dummy.ice_getEndpoints())

    def test_ping(self):
        self.assertEquals(self.sut.ping(), None)
