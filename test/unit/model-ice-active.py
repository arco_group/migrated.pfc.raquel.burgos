# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.append('test')
sys.path.insert(0, '.')
import threading
import time

from unittest import TestCase
from mock import Mock

import json

import Ice, IceStorm
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice"))
import DUO
Ice.loadSlice("-I/usr/share/slice/duo -I{0} --all {1}".format(Ice.getSliceDir(),
                                                               "/usr/share/slice/duo/dummy/dummy.ice"))
import DummyDUO

from inspectio.models import ActiveModel

class BoolRWA_I(DummyDUO.IBool.RWA):
    def __init__(self):
        self.observer = None

    def getObserver(self, current):
        return self.observer

    def setObserver(self, observer, current):
        self.observer = observer

class ByteRWA_I(DummyDUO.IByte.RWA):
    def __init__(self):
        self.observer = None

    def getObserver(self, current):
        return self.observer

    def setObserver(self, observer, current):
        self.observer = observer

class Controller(object):
    pass

class TestActiveModel(TestCase):
    def setUp(self):
        argv = ['--Ice.Config=config/local.config']
        self.ic = Ice.initialize(argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.adapter.activate()

    def newDummyBool(self):
        self.servant = BoolRWA_I()
        self.dummyBool = self.adapter.addWithUUID(self.servant)

    def newDummyByte(self):
        self.servant = ByteRWA_I()
        self.dummyByte = self.adapter.addWithUUID(self.servant)


    def test_can_manageBool(self):
        self.newDummyBool()
        self.assertTrue(ActiveModel.can_manage(self.dummyBool))

    def test_set_observerBool(self):
        self.newDummyBool()
        observer = BoolRWA_I()
        dummyObserver = self.adapter.addWithUUID(observer)
        sut = ActiveModel(self.dummyBool)
        sut.set_observer(self.ic, dummyObserver)
        self.assertEquals(sut.get_observer(), dummyObserver)

    def test_can_manageByte(self):
        self.newDummyByte()
        self.assertTrue(ActiveModel.can_manage(self.dummyByte))

    def test_set_observerByte(self):
        self.newDummyByte()
        observer = ByteRWA_I()
        dummyObserver = self.adapter.addWithUUID(observer)
        sut = ActiveModel(self.dummyByte)
        sut.set_observer(self.ic, dummyObserver)
        self.assertEquals(sut.get_observer(), dummyObserver)
