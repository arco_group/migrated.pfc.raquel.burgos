# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.insert(0, '.')

from unittest import TestCase

from inspectio.views import ViewFactory,BoolView, ByteView, ObjectView, ContainerView, TopicManagerView
from inspectio.models import Model, BoolModel, ByteModel, ObjectModel, ContainerModel, TopicManagerModel

class TestViewFactory(TestCase):
    def setUp(self):
        ViewFactory.register('ByteModel', ByteView())
        self.viewByte = ViewFactory.create(ByteModel(None))
        self.viewByteList = {"Textbox" : "templates/textbox.html",
                             "Spinner": "templates/spinner.html" }
        ViewFactory.register('BoolModel', BoolView())
        self.viewBool = ViewFactory.create(BoolModel(None))
        self.viewBoolList = {"Checkbox":"templates/checkbox.html",
                              "Image":"templates/image_checkbox.html"}
        ViewFactory.register('ObjectModel', ObjectView())
        self.viewObject = ViewFactory.create(ObjectModel(None))
        self.viewObjectList = {"Object" : "templates/object.html"}

        ViewFactory.register('ContainerModel', ContainerView())
        self.viewContainer = ViewFactory.create(ContainerModel(None))

        ViewFactory.register('TopicManagerModel', TopicManagerView())
        self.viewTopicManager = ViewFactory.create(TopicManagerModel(None))


    def test_register_BoolView(self):
        ViewFactory.register('BoolModel', BoolView())
        self.assertTrue('BoolModel' in ViewFactory.views)

    def test_create_BoolView(self):
        ViewFactory.register('BoolModel', BoolView())
        self.assertTrue(ViewFactory.create(BoolModel(None)), 'BoolView')

    def test_register_ByteView(self):
        ViewFactory.register('ByteModel', ByteView())
        self.assertTrue('ByteModel' in ViewFactory.views)

    def test_create_ByteView(self):
        ViewFactory.register('ByteModel', ByteView())
        self.assertTrue(ViewFactory.create(ByteModel(None)), 'ByteView')

    def test_create_ObjectView(self):
        ViewFactory.register('ObjectModel', ObjectView())
        self.assertTrue(ViewFactory.create(ObjectModel(None)), 'ObjectView')

    def test_get_template_ByteView(self):
        self.assertEquals(self.viewByte.get_template(), "templates/textbox.html")

    def test_get_script_ByteView(self):
        self.assertEquals(self.viewByte.get_script(), "inspectio/templates/js/ByteDevice.js")

    def test_get_template_BoolView(self):
        self.assertEquals(self.viewBool.get_template(), "templates/checkbox.html")

    def test_get_script_BoolView(self):
        self.assertEquals(self.viewBool.get_script(), "inspectio/templates/js/BooleanDevice.js")

    def test_get_name_by_default_Bool(self):
        self.assertEquals(self.viewBool.get_name_by_default(), "Checkbox")

    def test_get_name_by_defautl_Byte(self):
        self.assertEquals(self.viewByte.get_name_by_default(), "Textbox")

    def test_get_list_templates_Bool(self):
        self.assertEquals(self.viewBool.get_list_templates(), self.viewBoolList)

    def test_get_list_templates_Byte(self):
        self.assertEquals(self.viewByte.get_list_templates(), self.viewByteList)

    def test_get_name_by_default_Object(self):
        self.assertEquals(self.viewObject.get_name_by_default(), "Object")

    def test_get_list_templates_Object(self):
        self.assertEquals(self.viewObject.get_list_templates(), self.viewObjectList)

    def test_get_script_ObjectView(self):
        self.assertEquals(self.viewObject.get_script(), "inspectio/templates/js/ObjectDevice.js")

    def test_register_ContainerView(self):
        ViewFactory.register('ContainerModel', ContainerView())
        self.assertTrue('ContainerModel' in ViewFactory.views)

    def test_create_ContainerView(self):
        ViewFactory.register('ContainerModel', ContainerView())
        self.assertTrue(ViewFactory.create(ContainerModel(None)), 'ContainerView')

    def test_get_template_ContainerView(self):
        self.assertEquals(self.viewContainer.get_template(), "templates/container.html")

    def test_register_TopicManagerView(self):
        ViewFactory.register('TopicManagerModel', TopicManagerView())
        self.assertTrue('TopicManagerModel' in ViewFactory.views)

    def test_create_TopicManagerView(self):
        ViewFactory.register('TopicManagerModel', ContainerView())
        self.assertTrue(ViewFactory.create(TopicManagerModel(None)), 'TopicManagerView')

    def test_get_template_TopicManagerView(self):
        self.assertEquals(self.viewTopicManager.get_template(), "templates/topic_list.html")
