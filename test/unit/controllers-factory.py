# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.append('test')

from unittest import TestCase
from tornado import ioloop, web, websocket, httpclient
import urllib
from commodity.os_ import SubProcess
from commodity.testing import wait_that, listen_port, localhost, assert_that
import Ice, IceStorm

Ice.loadSlice("-I /usr/share/slice -I{0} --all {1}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice"))
import DUO
Ice.loadSlice("-I/usr/share/slice/duo -I{0} --all {1}".format(Ice.getSliceDir(),
                                                               "/usr/share/slice/duo/dummy/dummy.ice"))
import DummyDUO
import inspectio.controllers

class BoolRWA_I(DummyDUO.IBool.RWA):
    def __init__(self):
        self.value = False

    def get(self, current):
        return self.value

    def set(self, value, oid, current):
        self.value = value

class TestControllerFactory(TestCase):
    def setUp(self):
        argv = ['--Ice.Config=config/local.config']

        # self.istaf = SubProcess("duo-istaf")
        # wait_that(localhost, listen_port(10000))

        # self.server = SubProcess(
        #     "python inspectio/server.py")

        # wait_that(localhost, listen_port(8090))

        self.ic = Ice.initialize(argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.adapter.activate()
        self.servant = BoolRWA_I()
        self.dummyBool = self.adapter.addWithUUID(self.servant)
        self.http_client = httpclient.HTTPClient()

        # self.addCleanup(self.server.terminate)
        # self.addCleanup(self.istaf.terminate)



#    def test_create_Controller(self):
        #websocket = ControllerFactory.create(args)
   #     self.assertEquals(
#        input_url = urllib.quote (str(self.dummyBool))
#        request = httpclient.HTTPRequest('http://127.0.0.1:8090/inspect-element/?proxy=Hello1%20-t:tcp%20-h%20127.0.0.1%20-p%208889', method='GET', body='', headers={'Origin': 'http://localhost:8090', 'Upgrade': 'websocket', 'Sec-Websocket-Extensions': 'x-webkit-deflate-frame', 'Sec-Websocket-Version': '13', 'Connection': 'Upgrade', 'Sec-Websocket-Key': 'NK5mG5qi9hOVf9/CQQu5xg==', 'Host': 'localhost:8090', 'Pragma': 'no-cache', 'Cache-Control': 'no-cache'})
       # response = self.http_client.fetch('http://127.0.0.1:8090/inspect-element/?proxy=Hello1%20-t:tcp%20-h%20127.0.0.1%20-p%208889')
#        response = self.http_client.fetch(request)
#        self.assert_(ControllerFactory.__call__.called)
