# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.append('test')


from unittest import TestCase

import Ice, IceStorm
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice"))
Ice.loadSlice("-I/usr/share/slice/duo -I{0} --all {1}".format(Ice.getSliceDir(),
                                                               "/usr/share/slice/duo/dummy/dummy.ice"))


import duo
import DummyDUO

from inspectio.models import ModelFactory, BoolModel, ByteModel, ObjectModel, ActiveModel, ContainerModel, TopicManagerModel, AsdaModel

def toListModels(models):
    retval = []
    for item in models:
        retval.append(item.__class__.__name__)
    return retval

class BoolRWA_I(DummyDUO.IBool.RWA):
    def __init__(self, proxy):
        self.value = False

    @classmethod
    def can_manage(cls, proxy):
        return True

class TestModelFactory(TestCase):
    def setUp(self):
        argv = ['--Ice.Config=config/local.config']
        self.ic = Ice.initialize(argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.servant = BoolRWA_I(None)
        self.dummy = self.adapter.addWithUUID(self.servant)
        self.adapter.activate()

    def test_resgister_BoolModel(self):
        ModelFactory.register(BoolModel)
        self.assertTrue(BoolModel in ModelFactory.models)

    def test_register_ByteModel(self):
        ModelFactory.register(ByteModel)
        self.assertTrue(ByteModel in ModelFactory.models)

    def test_register_ObjectModel(self):
        ModelFactory.register(ObjectModel)
        self.assertTrue(ObjectModel in ModelFactory.models)

    def test_register_ActiveModel(self):
        ModelFactory.register(ActiveModel)
        self.assertTrue(ActiveModel in ModelFactory.models)

    def test_register_ContainerModel(self):
        ModelFactory.register(ContainerModel)
        self.assertTrue(ContainerModel in ModelFactory.models)

    def test_register_TopicManagerModel(self):
        ModelFactory.register(TopicManagerModel)
        self.assertTrue(TopicManagerModel in ModelFactory.models)

    def test_register_AsdaModel(self):
        ModelFactory.register(AsdaModel)
        self.assertTrue(AsdaModel in ModelFactory.models)

    def test_create_Model(self):
        models = ModelFactory.create(self.dummy)
        listModels = toListModels(models)
        self.assertEquals(listModels, ['BoolModel', 'ActiveModel', 'ObjectModel'])

    def test_model_is_a_singleton(self):
        model1 = ModelFactory.create(self.dummy)
        model2 = ModelFactory.create(self.dummy)
        self.assertEquals(model1, model2)
