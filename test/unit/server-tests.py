# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.append('test')
sys.path.insert(0, '.')

from unittest import TestCase
from requests import HTTPError
from commodity.os_ import SubProcess
from commodity.testing import wait_that, listen_port, localhost, assert_that

import Ice, IceStorm
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice"))
import DUO

from tornado import ioloop, web, websocket, httpclient
from inspectio import models, views, controllers, server

dummies={"Hello": "Hello -t:tcp -h 127.0.0.1 -p 9999",
         "Byte": "Byte -t:tcp -h 127.0.0.1 -p 9992"}
class TestServer(TestCase, web.RequestHandler):
    def setUp(self):

        self.istaf = SubProcess("duo-istaf")

        wait_that(localhost, listen_port(10000))

        self.server = SubProcess(
            "python inspectio/server.py")

        wait_that(localhost, listen_port(8090))

        self.http_client = httpclient.HTTPClient()

        key = "IceStorm.TopicManager.Proxy"
        self.ic = Ice.initialize(['--Ice.Config=config/local.config'])
        proxyIceStorm = self.ic.propertyToProxy(key)
        self.topic_mng = IceStorm.TopicManagerPrx.checkedCast(proxyIceStorm)

        self.addCleanup(self.server.terminate)
        self.addCleanup(self.istaf.terminate)

    def test_DirectoryHandler(self):
        response = self.http_client.fetch('http://127.0.0.1:8090/')
        self.failIf(response.error)
        self.assertTrue(response.body.find("<title>INSPECTIO</title>"))

    def test_ObjectProxyHandlerBool(self):
        response = self.http_client.fetch('http://127.0.0.1:8090/handler/?proxy=Hello+-t%3Atcp+-h+127.0.0.1+-p+9999&view=default&model=default')
        self.failIf(response.error)
        self.assertTrue(response.body.find('Model: BoolModel View: Checkbox'))

    def test_ObjectProxyHandlerByte(self):
        response = self.http_client.fetch('http://127.0.0.1:8090/handler/?proxy=Byte+-t%3Atcp+-h+127.0.0.1+-p+9992&view=default&model=default')
        self.failIf(response.error)
        self.assertTrue(response.body.find('Model: ByteModel View: Textbox'))

    def test_BadHandler(self):
        self.assertRaises(HTTPError("http://127.0.0.1:8090/algo", 404, "Not Found"))

    def test_UnknownProxy(self):
        response = self.http_client.fetch('http://127.0.0.1:8090/handler/?proxy=Byte6+-t%3Atcp+-h+127.0.0.1+-p+34445&view=default&model=default')
        self.failIf(response.error)
        self.assertFalse(response.body==0)

    def test_BadProxy(self):
        response = self.http_client.fetch('http://127.0.0.1:8090/handler/?proxy=BadProxy&view=default&model=default')
        self.failIf(response.error)
        self.assertFalse(response.body==0)

    def test_ChangeViewHandler(self):
        response = self.http_client.fetch('http://127.0.0.1:8090/handler/?proxy=Hello+-t%3Atcp+-h+127.0.0.1+-p+9999&view=Switch&model=default')
        self.failIf(response.error)
        self.assertTrue(response.body.find('Model: BoolModel View: Switch'))

    def test_ChangeModelHandler(self):
        response =  self.http_client.fetch('http://127.0.0.1:8090/handler/?proxy=Hello+-t%3Atcp+-h+127.0.0.1+-p+9999&view=default&model=ObjectModel')
        self.failIf(response.error)
        self.assertTrue(response.body.find("['::DUO::Active::R', '::DUO::Active::W', '::DUO::IBool::R', '::DUO::IBool::W', '::DummyDUO::IBool::RW', '::DummyDUO::IBool::RWA', '::Ice::Object']"))

    def test_no_arguments(self):
        response = self.http_client.fetch("http://127.0.0.1:8090/handler/?proxy=Hello+-t%3Atcp+-h+127.0.0.1+-p+9999")
        self.failIf(response.error)
        self.assertFalse(response.body==0)

    def test_ViewTopicManager(self):
        response = self.http_client.fetch("http://127.0.0.1:8090/handler/?proxy=ISTopicAsFacet/TopicManager%20-t:tcp%20-h%20127.0.0.1%20-p%2011000&view=default&model=default")
        self.failIf(response.error)
        self.assertTrue(response.body.find('<div id="div-left" name ="TopicManager">'))
