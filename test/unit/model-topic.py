# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.append('test')
sys.path.insert(0, '.')
import threading
from unittest import TestCase
import Ice, IceStorm

from commodity.os_ import SubProcess
from commodity.testing import wait_that, listen_port, localhost, assert_that

from mock import Mock
from inspectio.models import AsdaModel

Ice.loadSlice("-I /usr/share/slice -I{0} --all {1} {2}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice",
                                                           "/usr/share/slice/duo/ASDF.ice"))
import DUO, ASD

class Asda_I(ASD.Listener):
    def __init__(self):
        self.elements = {}

    def adv(self, prx, current):
        identity = prx.ice_getIdentity()
        self.elements[identity] = prx

    def bye(self, identity, current):
        del self.elements[identity]

    def getObserver(self, current):
        return None

class Controller(object):
    pass

class TestTopicModel(TestCase):
    def setUp(self):
        argv = ['--Ice.Config=config/local.config']
        self.istaf = SubProcess("duo-istaf")

        wait_that(localhost, listen_port(10000))

        self.ic = Ice.initialize(argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints('tcp', 'default')
        self.servant = Asda_I()

        self.dummy = self.adapter.addWithUUID(self.servant)
        self.adapter.activate()

        key = "IceStorm.TopicManager.Proxy"
        proxyIceStorm = self.ic.propertyToProxy(key)
        self.topic_mng = IceStorm.TopicManagerPrx.checkedCast(proxyIceStorm)

        self.proxy = self.topic_mng.create("Test")
        self.topic = AsdaModel(self.proxy)

        self.addCleanup(self.istaf.terminate)

    def test_can_manage(self):
        self.assertTrue(AsdaModel.can_manage(self.proxy))

    def test_get_name(self):
        self.assertEquals(self.topic.getName(), "Test")

    def test_get_publisher(self):
        self.assertEquals(self.topic.getPublisher(), self.proxy.getPublisher())

    def test_destroy(self):
        proxy = self.topic_mng.create("Test4")
        topic = AsdaModel(proxy)
        topic.destroy()
        self.assertRaises(IceStorm.NoSuchTopic, self.topic_mng.retrieve, "Test4" )

    def test_listener_is_a_singleton(self):
        proxy = "ASDA -t:tcp -h 127.0.0.1 -p 11000"
        self.topic_mng.create("ASDA")
        asda = AsdaModel(self.ic.stringToProxy(proxy))
        asda.setup(self.adapter)
        listener1 = asda.listener

        asda.setup(self.adapter)
        listener2 = asda.listener

        self.assertEquals(listener1, listener2)


    # def test_controller_invoked(self):
    #     event = threading.Event()

    #     proxy = "TopicTest -t:tcp -h 127.0.0.1 -p 11000"
    #     self.topic_mng.create("TopicTest")
    #     topic = TopicModel(self.ic.stringToProxy(proxy))
    #     topic.setup(self.adapter)


    #     controller = Controller()
    #     controller.write_message = Mock()
    #     controller.write_message.side_effect = lambda x: event.avd()
    #     topic.attach(controller)

    #     tp = IceStorm.TopicManagerPrx.checkedCast(topic.getPublisher())
    #     tp.adv("TopicTest -t:tcp -h 127.0.0.1 -p 11000")


    #     event.wait(1)

    #     self.assert_(controller.write_message.called)
    #     response = unmarshall(controller.write_message.call_args[0][0])
    #     self.assertEquals(response[0], "TopicTest -t:tcp -h 127.0.0.1 -p 11000" )
