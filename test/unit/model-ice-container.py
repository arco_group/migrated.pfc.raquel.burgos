# -*- coding:utf-8; tab-width:4; mode:python -*-
import sys
sys.path.append('test')
sys.path.insert(0, '.')
import threading
import time

from unittest import TestCase
from hamcrest import is_not
from commodity.os_ import SubProcess, check_output
from commodity.testing import wait_that, listen_port, localhost, assert_that
from mock import Mock

import json

import Ice, IceStorm
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1} {2}".format(
        Ice.getSliceDir(),
        "/usr/share/slice/duo/DUO.ice",
        "inspectio/ContextService/ContextService.ice"))


import DUO, ContextService

Ice.loadSlice("-I/usr/share/slice/duo -I{0} --all {1}".format(Ice.getSliceDir(),
                                                               "/usr/share/slice/duo/dummy/dummy.ice"))
import DummyDUO

from inspectio.models import ContainerModel

env = {'LD_LIBRARY_PATH': 'src/'}

class TestContainerModel(TestCase):
    def setUp(self):
        check_output('mkdir -p /tmp/db')
        check_output('mkdir -p /tmp/psdb')
        check_output('mkdir -p /tmp/csdb')

        argv = ['--Ice.Config=config/local.config']

        self.icestorm = SubProcess(
            "icebox --Ice.Config=config/icebox.IS.config")

        self.contextserver = SubProcess(
            'icebox --Ice.Config=config/icebox-example.cfg', env=env)

        wait_that(localhost, listen_port(20002))

        self.ic = Ice.initialize(argv)

        self.base = self.ic.propertyToProxy("ContextServer.Proxy")

        # Retrieve a proxy
        self.dummy = ContextService.ContainerPrx.checkedCast(self.base)
        self.sut = ContainerModel(self.dummy)
        self.addCleanup(self.contextserver.terminate)
        self.addCleanup(self.icestorm.terminate)

    def tearDown(self):
        for ch in self.sut.list().keys():
            self.sut.unlink(ch)
        self.sut.destroy()

    def test_can_manage(self):
        self.assertTrue(ContainerModel.can_manage(self.dummy))

    def test_list(self):
        oid = self.ic.stringToIdentity('test')
        prx = self.dummy.ice_identity(oid)
        self.sut.link('Children1', prx)
        self.assertTrue('Children1' in self.sut.list().keys())

    def test_link(self):
        oid = self.ic.stringToIdentity('test')
        prx = self.dummy.ice_identity(oid)
        self.sut.link('Children', prx)
        self.assertEquals(len(self.sut.list()), 1)
        self.assertTrue('Children' in self.sut.list().keys())
        print self.sut.list().keys()

    def test_unlink(self):
        oid = self.ic.stringToIdentity('test')
        prx = self.dummy.ice_identity(oid)
        self.sut.link('Children2', prx)
        self.sut.unlink('Children2')
        self.assertEquals(len(self.sut.list()),0)


    def test_create(self):
        self.sut.create('ChildrenContainer')
        self.assertTrue(ContainerModel.can_manage(self.sut.list()['ChildrenContainer']))

    def test_destroy(self):
        children = self.sut.create('ChildrenContainer')
        self.assertTrue('ChildrenContainer' in self.sut.list().keys())
        children.destroy()
        self.assertRaises(Ice.ObjectNotExistException,
                           children.ice_ids)
        print self.sut.list()
