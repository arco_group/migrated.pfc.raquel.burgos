#!/usr/bin/python
import time
import sys
import Ice, IceStorm

Ice.loadSlice("-I /usr/share/slice -I{0} --all {1} {2}".format(Ice.getSliceDir(),
                                                           "/usr/share/slice/duo/DUO.ice",
                                                           "/usr/share/slice/duo/ASDF.ice"))
import DUO, ASD

class Publisher(Ice.Application):
    def get_topic_manager(self):
        key = 'IceStorm.TopicManager.Proxy'
        proxy = self.communicator().propertyToProxy(key)
        if proxy is None:
            print "property", key, "not set"
            return None

        print("Using IceStorm in: '%s'" % key)
        return IceStorm.TopicManagerPrx.checkedCast(proxy)

    def run(self, argv):
        topic_mgr = self.get_topic_manager()
        if not topic_mgr:
            print ': invalid proxy'
            return 2

        topic_name = "ASDA"
        try:
            topic = topic_mgr.retrieve(topic_name)
        except IceStorm.NoSuchTopic:
            print "no such topic found, creating"
            topic = topic_mgr.create(topic_name)

        publisher = topic.getPublisher()
        printer = ASD.ListenerPrx.uncheckedCast(publisher)

        print "Subscribiendo elementos al canal de anunciamientos"

        hello1 = "Hello1 -t:tcp -h 127.0.0.1 -p 8889";
        printer.adv(self.communicator().stringToProxy(hello1))
        time.sleep(2)
        byte1 = "Byte1 -t:tcp -h 127.0.0.1 -p 8892";
        printer.adv(self.communicator().stringToProxy(byte1))

        return 0


sys.exit(Publisher().main(sys.argv))
