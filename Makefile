check:

	atheist test/integration/*.test -veo
	nosetests test/unit/*.py


clean:
	find -name "*~" | xargs rm -v
	find -name "*.pyc" | xargs rm -v

run_istaf:
	duo-istaf &

run_contextservice:
	icebox --Ice.Config=config/icebox-contextService.cfg &

run_launcher:
	python launch-container.py --Ice.Config=config/local.config

kill:
	pkill -USR1 icebox
