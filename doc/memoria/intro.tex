% -*- coding: utf-8 -*-

\chapter{Introducción}
\label{chap:intro}

En la actualidad, las personas no son los únicos usuarios de Internet. Desde el año 2008, el número de \emph{cosas} conectadas a la red superó al número de personas que habitan la Tierra.

Los usos que se pueden dar a la interconexión de los objetos a través de Internet son tan
dispares que van desde el control de varios dispositivos que conforman una red distribuida
a pequeña escala, hasta sistemas de control y vigilancia de amplia cobertura.

Sea cual sea la aplicación que ofrezcan esos usos, están todos englobados dentro de la
tendencia que se conoce como el \acf{IoT}, en castellano el \emph{Internet de las cosas}.

\section{Internet of Things (\acs{IoT})}
\label{sec:intro:iot}

El término se atribuye a Kevin Ashton para referirse a una Internet que incluya a las
personas y sus cosas identificadas con etiquetas \acs{RFID}~\cite{ashton}. Hasta ahora ha
sido esa tecnología, junto con la de los códigos \acs{QR}~\cite{qr} las que han abarcado
de manera más amplia las aplicaciones desarrolladas dentro de esta filosofía. Sin embargo,
el impulso del hardware libre como la plataforma Arduino\cite{arduino}, o la adopción del
nuevo protocolo de Internet (\acs{IPv6})~\cite{ipv6}, permitirán que sean cada vez más los
objetos con identidad propia en la red, y que el número de aplicaciones para
identificarlos, localizarlos y trabajar con ellos se multiplique.

\subsection{Aplicaciones del \acs{IoT}}
\label{sec:intro:iot:aplicaciones}

Para ilustrar el concepto de \acs{IoT}, en esta sección se describen brevemente algunos
proyectos y propuestas relacionados.

\subsubsection{Smartcities: SmartSantander}

Se prevee que para el año 2025 el 70\% de la población mundial
vivirá en las ciudades. En la actualidad, las ciudades consumen el
75\% de los recursos y de la energía mundial y generan el 80\% de los
gases responsables del efecto invernadero, ocupando tan sólo el 2\%
del territorio mundial \cite{necesariasSmartCities}. Estos datos ponen
de manifiesto la necesidad de gestionar de manera eficiente y sostenible los
recursos disponibles, y es aquí donde el uso de las nuevas tecnologías
se hace imprescindible.

Se define \emph{Smart City} (en castellano Ciudad Inteligente)
como aquel núcleo urbano que usa las tecnologías de la información y
las comunicaciones para hacer que tanto su infraestructura crítica,
como sus componentes y servicios públicos ofrecidos sean más
interactivos, eficientes y los ciudadanos puedan ser más conscientes
de ellos.  Es una ciudad comprometida con su entorno, tanto desde el
punto de vista medioambiental como en lo relativo a los elementos
culturales e históricos \cite{definicionSmartCity}.

\emph{SmartSantander}\cite{smartSantander} es un proyecto de la
Universidad de Cantabria (UC) junto con Telefónica I+D que convierte
este municipio español en la primera ciudad inteligente integral de
Europa.

En septiembre de 2013 habrá distribuidos 20.000 dispositivos
inalámbricos (captadores, actuadores, cámaras, terminales
móviles,etc.) a lo largo de la ciudad. Estos dispositivos recogerán
información de todos los puntos del municipio con múltiples
aplicaciones, entre las que se encuentran las de disponer en tiempo
real de la información de las plazas libres de aparcamiento, el
análisis de los parámetros ambientales como el ruido o la emisión de
dióxido de carbono, o las necesidades de riego de los parques y
jardines.

\subsubsection{Domótica y las Smart-Homes: SmartThings}

Se entiende por \emph{Domótica} el conjunto de sistemas que permiten
la automatización de distintos elementos de un edificio con el fin de
gestionar eficientemente el consumo energético, aumentar la
seguridad, el bienestar de las personas, etc.

El término Domótica no es algo nuevo, sin embargo, hasta ahora venía
ligado a un elevado coste de implantación y mantenimiento, que lo
convertía en algo exclusivo y poco difundido. Con la
aparición de nuevos dispositivos más económicos y las nuevas
tecnologías nombradas anteriormente, cada vez son más las aplicaciones
que permiten a los usuarios crear sus propias redes de dispositivos
domóticos a un precio más asequible, como es el caso de
\emph{SmartThings}\cite{smartThings}.

La tecnología de SmartThing está basada en un router y una serie de
sensores e interruptores inteligentes que se conectan inalámbricamente
con este router y que pueden ser monitorizados a través de un conjunto
de aplicaciones para el \emph{Smartphone} del
cliente llamadas \emph{SmartApps}.

La ventaja de este producto es que cualquier cosa que el usuario tenga
en casa puede convertirse en \emph{inteligente} gracias a los diferentes kits que
proporcionan los desarrolladores. Dentro de estos kits se integran
enchufes de corriente y sensores de presencia y movimiento, pudiendo
controlar remotamente el encendido o apagado de cualquier cosa que se
pueda enchufar, controlar el estado de las puertas o ventanas, etc.,
todo ello a través de las distintas \emph{SmartApps}.

El usuario puede descargarse en su \emph{Smartphone} las aplicaciones
que más se ajusten a sus necesidades y a la configuración de sus
dispositivos o el kit adquirido.


\subsection{Representación de la información}
\label{sec:intro:iot:representacion}

En un mundo en el que millones de dispositivos se conectan a internet para acceder o dar a
conocer datos obtenidos a través de sus sensores, es importante la manera en la que esos
datos se comparten y representan para convertirse en información útil para las personas.

Aplicaciones como \emph{Vista Data Vision}\cite{vdv}, \emph{Pachube}\cite{pachube} o
\emph{Tales of Things}\cite{tales}, hacen uso de \emph{Google Maps}\cite{googleMaps} para
representar gráficamente los sensores u objetos distribuidos por todo el planeta y a
través de las páginas web de cada una de ellas permiten al usuario administrar, visualizar
y analizar los datos obtenidos de los dispositivos en tiempo real.

A pequeña escala, cuando se trata de controlar elementos del entorno del propio usuario,
las empresas proporcionan soluciones en forma de \emph{App} para dispositivos móviles,
como es el caso de \emph{SmartThings}\cite{smartThings} para configurar y controlar una
red domótica, o el caso de \emph{Nike+}\cite{nike+}, que a través de sensores en las
zapatillas envía información a través de \emph{Bluetooth}\cite{bluetooth} al móvil para
hacer un seguimiento preciso del entrenamiento, la distancia recorrida, las calorías
quemadas, etc.

Aunque la tendencia de los fabricantes es proporcionar facilidades para poder utilizar sus
sistemas independientemente de las tecnologías que emplee el usuario final (tipo de
dispositivo, Sistema Operativo...), hasta ahora muchas aplicaciones cuentan con
limitaciones e incompatibilidades de este tipo.

La finalidad de este proyecto será conseguir desacoplar los objetos de los
dispositivos y tecnologías que interactuarán con ellos, de manera que el usuario
pueda configurar su propia red de sensores y actuadores, y pueda acceder a cada
uno de ellos remotamente a través de un navegador con independencia del hardware
o el \acs{SO} con el que esté trabajando.


\section{Estructura del documento}
\label{sec:intro:estructura}
En esta sección se enumeran los diferentes capítulos que componene este documento junto
con una breve descripción de cada uno de ellos.
\begin{definitionlist}
\item[Capítulo 2: «Antecedentes»]

En este capítulo se introducirán las diferentes tecnologías existentes para la
comunicación asíncrona en la web, describiendo cada una de ellas
y analizando los pros y contras de su utilización en este proyecto. Asimismo, se detallarán los
frameworks de pruebas para JavaScript estudiados y empleados.

Se introducirán brevemente los middlewares de comunicaciones orientados a
objetos, haciendo especial hicapié en el middleware \emph{ZeroC Ice}~\cite{ice}, que será
el empleado durante el desarrollo del trabajo.

Por último, se describirán el modelo de información \acs{DUO} y el servicio \acs{ASD},
ambos utilizados para la comunicación del sistema con los dispositivos remotos.

\item[Capítulo 3: «Objetivos»]
El objetivo del proyecto es el desarrollo de una herramienta dinámica y completamente
flexible, para la monitorización e interacción con los distintos elementos que conformen
una red de objetos distribuidos con independencia de las tecnologías empleadas por el
usuario final.

En este capítulo se describirán con más detalle los objetivos específicos que se pretenden
alcanzar a lo largo del ciclo de vida del proyecto.

\item[Capítulo 4: «Método de trabajo y herramientas»]
Para el desarrollo de este trabajo se ha seguido la metodología del \emph{Prototipado
  Incremental} junto con la técnica \acf{TDD} para su implementación. En este apartado de
abordarán brevemente los principios de dicha metodología y se expondrán las herramientas,
tanto hardware como software, utilizadas durante todo el proceso de realización del
mismo.

\item[Capítulo 5: «Desarrollo del proyecto»]
En esta sección se analizarán los requisitos funcionales del proyecto para obtener los
casos de uso correspondientes y se describirá brevemente el patrón de arquitectura
seleccionado para la implementación del sistema. Posteriormente se detallará cada una de
las iteraciones en las que se ha dividido el desarrollo del proyecto. En cada una de ellas
se describirán el análisis de los requisitos y el diseño planteado y, a continuación, se
desarrollará más apliamente la implementación del código obtenido.

\item[Capítulo 6: «Resultados»]
En este capítulo se realizará un resumen del sistema final obtenido y sus
aplicaciones. Asimismo, se analizará el esfuerzo de desarrollo del proyecto y del tiempo
dedicado al mismo, así como una estimación del coste total que ha supuesto su
realización.

\item[Capítulo 7: «Conclusiones y trabajo futuro»]
Por último, en este apartado se analizarán los objetivos conseguidos a lo largo del
desarrollo de este proyecto. Además, se detallarán algunas sugerencias de trabajo futuro
para ampliar y mejorar la funcionalidad del modelo presentado.

\end{definitionlist}
