\chapter{Resultados}
\label{cha:resultados}

En esta sección se detallará el sistema final obtenido tras el desarrollo explicado en la sección \ref{sec:desarrollo:implementacion} y las aplicaciones del mismo. Asimismo, se analizará el esfuerzo de desarrollo del proyecto y del tiempo dedicado al mismo, y se realizará una estimación del coste total que ha supuesto su realización.

\section{Aplicaciones de la herramienta}
\label{sec:resultados:aplicaciones}

El objetivo principal de este proyecto era obtener una herramienta capaz de monitorizar y controlar objetos de una red distribuida a través de internet con independencia de las tecnologías empleadas por el usuario final. Esto se ha conseguido desacoplando la implementación del sistema de cualquier tipo de Sistema Operativo o dispositivo concreto, pudiendo trabajar con la herramienta desde cualquier navegador web que soporte WebSockets, con independencia de las características del terminal.

A continuación se detallan las aplicaciones facilitadas por el sistema.

\subsection{Inspección y control de dispositivos remotos}
\label{sec:resultados:aplicaciones:inspeccion}

Partiendo de una red de objetos distribuidos, el usuario será capaz de monitorizar el estado o valor de cada uno de ellos y tendrá la posibilidad de modificarlo remotamente interactuando de manera sencilla desde las vistas o páginas web que los representan.

\subsubsection{Objetos simples}

Los dispositivos soportados por el sismema pertenecen a dos tipos:

\begin{itemize}
\item Objetos booleanos: su estado puede tener dos únicos valores (\emph{true} o \emph{false}).
\item Objetos enteros: su estado o valor varía en un rango entre 0 y 255.
\end{itemize}

El usuario que previamente haya cargado en su navegador la vista correspondiente a un dispositivo de cualquiera de estos tipos podrá visualizar los cambios en su estado en tiempo real o modificarlo personalmente cuando desee (imagen \ref{fig:basic-bool-byte}).

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/Basic-bool-byte.png}
  \caption{Vistas básicas de un objeto entero (izquierda) y otro booleano (derecha)}
  \label{fig:basic-bool-byte}
\end{figure}

\subsubsection{Contenedores}

El sistema soporta además la inspección de contenedores de objetos. El usuario será capaz de comprobar qué elementos o dispositivos forman parte de un contenedor concreto, y podrá añadir nuevos objetos al mismo (imagen \ref{fig:basic-container}).

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/Basic-container.png}
  \caption{Vistas básicas de un objeto entero (izquierda) y un contenedor del que forma parte (derecha)}
  \label{fig:basic-container}
\end{figure}

\subsubsection{Objetos activos}

La herramienta final da soporte al usuario para poder consultar el observador de un objeto
activo y asignarle uno nuevo. Esta aplicación es útil si se desea difundir un cambio de
estado entre dispositivos del mismo tipo (ver imagen \ref{fig:basic-active-object}
izquierda).

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/Basic-active-object.png}
  \caption{Vistas básicas de un Modelo Activo (izquierda) y de un Modelo Objeto (derecha)}
  \label{fig:basic-active-object}
\end{figure}

\subsubsection{Propiedades generales de los objetos}

El usuario podrá también consultar en cualquier momento propiedades específicas de un
determinado dispositivo, como su Identidad, su Proxy o las interfaces que implementa (ver
imagen \ref{fig:basic-active-object} derecha).

\subsection{Monitorización de canales de eventos}
\label{sec:resultados:aplicaciones:canales}

Los dispositivos remotos se comunican entre sí y con el sistema a través de canales de
eventos. El usuario cuenta con la facilidad que le proporciona la herramienta para
monitorizar y subscribirse a algunos de los canales más representativos.

\subsubsection{Topic Manager}

Como se explicó en la sección \ref{sec:desarrollo:implementacion:canal_anunciamientos}, a
través de la vista del Topic Manager el usuario podrá conocer la lista de canales de
eventos activos en un determinado momento. Podrá a su vez, cargar la vista de alguno de
esos canales para inspeccionar los eventos o mensajes que le van llegando
(imagen \ref{fig:basic-topicManager-Asda} izquierda) y subscribir un objeto a cualquier
canal arrastrando el proxy sobre su nombre.


\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/Basic-topicManager-Asda.png}
  \caption{Vistas del TopicManager (izquierda) y del Canal de Anunciamientos (derecha)}
  \label{fig:basic-topicManager-Asda}
\end{figure}

\subsubsection{Canal de Anunciamientos}

Si el usuario no conoce el proxy de los objetos que componen la red, el sistema le
proporciona la posibilidad de monitorizar el canal de anunciamientos, donde se irán
añadiendo los proxies a los dispositivos que se vayan anunciando cuando su vista esté
cargada. De esta manera el usuario podrá acceder a multitud de objetos según vayan
incorporándose al sistema sin necesidad de conocer nada de ellos (imagen
\ref{fig:basic-topicManager-Asda} derecha).

\section{Recursos y costes}
\label{sec:recursos}

A continuación, se muestra un resumen de las líneas de código fuente asociadas a los
lenguajes de programación que se han utilizado en el desarrollo de este proyecto (ver
cuadro~\ref{tab:lineasporlenguaje}). 

\begin{table}[!h]
  \centering
  \input{tables/lineascodigo.tex}
  \caption{Líneas de código por lenguaje de programación}
  \label{tab:lineasporlenguaje}
\end{table}

En el cuadro~\ref{tab:lineasporelemento} se detallan
el número de líneas por componente del sistema, así como el número de líneas dedicadas a
las pruebas y a los ejemplos que se realizaron durante el estudio de viabilidad de las
distintas tecnologías y herramientas empleadas.




Para la obtención de estos datos, se ha utilizado la herramienta libre \texttt{cloc}, que
analiza todos los ficheros existentes de un directorio en particular, y muestra, de una
manera clara, el número total de líneas de código, incluyendo información adicional como
el número de ficheros y de comentarios entre otros.

\newpage

\begin{table}
  \centering
  \input{tables/codigo_detalle.tex}
  \caption{Líneas de código por componente}
  \label{tab:lineasporelemento}
\end{table}

En el listado \ref{cod:costes} se muestra una estimación del esfuerzo realizado en el
desarrollo de este proyecto. Estos datos se han obtenido con la herramienta
\texttt{sloccount}~\cite{sloccount}, que analiza los ficheros de un directorio dado y
emplea el modelo COCOMO para obtener la estimación del tiempo y los costes necesarios para
desarrollar el sistema.

\begin{listing}[
  language = Bash,
  caption = {Estimación de los costes del proyecto},
  label = cod:costes]
Total Physical Source Lines of Code (SLOC)                = 6,123
Development Effort Estimate, Person-Years (Person-Months) = 1.36 (16.27)
 (Basic COCOMO model, Person-Months = 2.4 * (KSLOC**1.05))
Schedule Estimate, Years (Months)                         = 0.60 (7.21)
 (Basic COCOMO model, Months = 2.5 * (person-months**0.38))
Estimated Average Number of Developers (Effort/Schedule)  = 2.25
Total Estimated Cost to Develop                           = $ 183,103
 (average salary = $56,286/year, overhead = 2.40).
\end{listing}


\subsection{Repositorio}
\label{sec:recursos:repositorio}

El código del proyecto se encuentra alojado en \emph{bitbucket.org}~\cite{bitbucket}, que
es un servicio de alojamiento basado en web para proyectos. Para descargar el repositorio
se ejecuta la siguiente sentencia en un terminal:


\begin{listing}[
  language = python]
$ hg clone https://bitbucket.org/arco_group/pfc.inspectio
\end{listing} %$

Este servicio permite utilizar \emph{Mercurial}~\cite{mercurial} como sistema de control
de versiones. Lo que permite visualizar todos los cambios realizados en cada
actualización, permitiendo el correcto seguimiento del desarrollo del proyecto.
