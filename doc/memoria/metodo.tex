% -*- coding: utf-8 -*-

\chapter{Método de trabajo y herramientas}
\label{chap:metodology}

En esta sección se describen brevemente la metodología de desarrollo empleada, así como
las herramientas software y hardware utilizadas para la elaboración del proyecto.

\section{Metodología de trabajo}
\label{sec:metodologia}

Desde el inicio de este proyecto quedan especificados los requisitos fundamentales que
debe satisfacer el sistema. Estos requisitos se dividen en tareas que compondrán las
diferentes iteraciones del desarrollo. El resultado de cada iteración se irá añadiendo a
los requisitos satifechos en las iteraciones anteriores. Es por esto que la metodología de
desarrollo utilizada haya sido el \emph{Prototipado Incremental}, complementado con la
metodología \acs{TDD}.

\subsection{Prototipado Incremental}
\label{sec:metodologia:prototipado_incremental}

El \textbf{Prototipado Incremental}~\cite{ingenieria_software} se basa en la generación de
varios modelos parciales ejecutables del sistema, expuestos a los comentarios del usuario,
refinándolos a través de las distintas versiones hasta alcanzar el producto final. El
hecho de que el cliente reciba los prototipos funcionales ayuda a comprobar el
cumplimiento de todos los requisitos, pudiendo a su vez modificarlos o añadir alguno
nuevo.

El esquema general de esta metodología se muestra en la figura
\ref{fig:prototipado_incremental}~\cite{ingenieria_software}. En cada iteración, se
definen nuevas funcionalidades del sistema, se describen más detalladamente y se
desarrollan. El prototipo obtenido se integra en el sistema y se entrega al cliente.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{images/prototipado_incremental.jpg}
    \caption{Diagrama de flujo del prototipado incremental}
    \label{fig:prototipado_incremental}
  \end{center}
\end{figure}

Las ventajas de utilizar esta metodología se pueden resumir en:
\begin{itemize}
\item Los clientes no tienen que esperar a que se entregue el sistema completo para poder
  utilizarlo y sacarle provecho. El primer prototipo satisface los requisitos más críticos
  y se puede utilizar el sistema inmediatamente.
\item Como las principales funcionalidades se entregan en las primeras iteraciones, son la
  parte del sistema sobre el que más pruebas se realizan, aumentando la fiabilidad y
  robustez del sistema.
\item El desarrollo de prototipos intermedios hace más fácil determinar si los nuevos
  requisitos planteados en las posteriores iteraciones son correctos y viables.
\item El riesgo de errores es muy bajo y los fallos son localizados y solucionados más
  rápidamente. En cada iteración  no sólo se prueba el prototipo realizado en ese momento,
  se vuelven a pasar las pruebas de las iteraciones anteriores para asegurar que el nuevo
  prototipo cumple también con las funcionalidades ya desarrolladas.
\end{itemize}

A pesar de las ventajas que proporciona el hecho de que el cliente participe activamente
durante todo el proceso de desarrollo, también puede acarrear alguna desventaja, como que
los requisitos varíen continuamente por las nuevas funcionalidades que el cliente vaya
demandando. Este inconveniente puede dar lugar a un mantenimiento más costoso debido a los
cambios en las especificaciones.



\subsection{Desarrollo Dirigido por Pruebas}
\label{sec:metodologia:desarrollo_dirigido_pruebas}
El Desarrollo Dirigido por Pruebas, del inglés \acf{TDD}  es una práctica de programación
ágil basada en la relización de las pruebas antes de la propia escritura del código a
probar.

El algoritmo \acs{TDD} se limita a tres pasos:

\begin{itemize}
\item Escritura de la prueba para uno y sólo uno de los requisitos.
\item Implementación del código que satisfaga la prueba.
\item Refactorización del código obtenido hasta el momento para eliminar duplicidades.
\end{itemize}

Esta técnica cuenta con mútiples ventajas respecto a técnicas más
tradicionales. Destacando entre ellas:

\begin{itemize}
\item El código implementado es el mínimo necesario para satisfacer un requisito. No hay
  código innecesario.
\item Los errores se detectan y localizan muy fácilmente.
\item Se obtiene un código altamente reutilizable.
\item El tiempo dedicado a depuración desaparece.
\end{itemize}

Gracias a estas ventajas, desde el inicio del proyecto se optó por esta técnica para
llevar a cabo la su implementación.

\section{Herramientas}
\label{sec:herramientas}

\subsection{Lenguajes de Programación}
\label{sec:herramientas:lenguajes}

\begin{description}
\item[Python] - lenguaje de programación interpretado, multiparadigma, orientado a objetos
  y multiplataforma. Empleado para la implementación del servidor web, los modelos y los
  controladores de los objetos~\cite{python}.

\item[HTML] - lenguaje de marcado para el desarrollo de páginas web utilizado para la
  implementación de las vistas~\cite{html}.

\item[JavaScript] - lenguaje de programación interpretado, orientado a objetos, que se
  utiliza principalmente para el lado del cliente en aplicaciones web~\cite{javascript}.

\end{description}

\subsection{Aplicaciones de Desarrollo}
\label{sec:herramientas:aplicaciones}

\begin{description}
\item[ZeroC Ice] - middleware de comunicación orientado a objetos desarrollado por la
  empresa ZeroC~\cite{ice}.

\item[GNU Emacs] - editor de texto utilizado para la programación y documentación del
  proyecto~\cite{emacs}.

\item[Eclipse] - entorno de programación~\cite{eclipse} utilizado para la realización de
  las pruebas unitarias del código JavaScript mediante el plugin
  \emph{jsTestDriver}~\cite{jstestdriver}.

\item[Mercurial] - sistema de control de versiones multiplataforma empleado durante todo
  el desarrollo del proyecto~\cite{mercurial}.

\item[Atheist] - framework de pruebas utilizado para la elaboración de las pruebas de
  integración del proyecto~\cite{atheist}.

\item[Dia] - herramienta de propósito general destinada a la creación de diagramas,
  utilizado para la creación de los diagramas que se muestran en la documentación
  final~\cite{dia}.

\item[GNU Make] - herramienta para la generación automática de ejecutables~\cite{make}.


\end{description}

\subsection{Documentación}
\label{sec:herramientas:documentacion}

\begin{description}
\item[\LaTeX] - lenguaje de marcado de documentos de carácter técnico y
  científico, utilizado para la elaboración de este documento~\cite{latex}.

\item[BibTex] - herramienta para generar las listas de referencias de documentos escritos
  con \LaTeX.
\end{description}


\subsection{Gestión de proyecto}
\label{sec:herramientas:gestion_proyecto}

\begin{description}
\item[Redmine] - Aplicación web para la gestión de proyectos. Mediante esta herramienta se
  han establecido las tareas a realizar a lo largo del ciclo de vida del
  proyecto~\cite{redmine}.

\end{description}

\subsection{Recusos Hardware}
\label{sec:herramientas:recursos_hardware}

\begin{itemize}
\item Procesador: Intel Core 2 Quad \acs{CPU} Q6600 @ 2.40GHz x4
\item Memoria: 3,9 GiB
\end{itemize}
