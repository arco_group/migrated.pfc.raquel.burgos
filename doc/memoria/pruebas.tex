\chapter{Pruebas}
\label{cha:pruebas}

Como se indicó en la sección \ref{sec:metodologia:desarrollo_dirigido_pruebas}, el
desarrollo del proyecto ha seguido la metodología \acs{TDD}, donde las pruebas cobran un
protagonismo especial, al ser el punto de partida para llegar a implementar toda
funcionalidad de manera robusta y fiable, y con la obtención del código mínimo que
satisface todos los requisitos.

A continuación se detallan las pruebas realizadas para la consecución de los diferentes
casos de uso del sistema.

\section{Registro y creación de modelos}
\label{sec:pruebas:registro_creacion_modelos}

La base del sistema es crear una representación virtual de un dispositivo físico para
poder interactuar con él. Partiendo del proxy o referencia a dicho dispositivo, la
\emph{Factoría de Modelos} debe encargarse de llevar a cabo este proceso.

Se realizan por tanto pruebas que comprueben el registro satisfactorio de nuevos tipos de
modelos, y la creación de modelos de diferentes tipos de objetos.

\subsection{Registro de un nuevo modelo}
\label{sec:pruebas:registro_creacion_modelos:registro}

Para poder analizar un proxy y crear un modelo que implemente sus mismas interfaces, antes
la Factoría debe tener un registro de todos los modelos soportados por el sistema.

Dado un nuevo modelo, se registra en la Factoría, y posteriormente se comprueba que,
 efectivamente, ha pasado a formar parte del registro general de modelos.

\subsection{Creación de un modelo dado un proxy}
\label{sec:pruebas:registro_creacion_modelos:creacion}

Como se ha explicado a lo largo de este documento, un dispositivo puede implementar
diferentes interfaces que se traducen en diferentes modelos internos de representación. La
Factoría ha de ser capaz de crear tantos modelos compatibles con el objeto como modelos
compatibles tenga registrados.

\begin{description}
\item[Dado] - un proxy a un objeto que no está representado en el sistema.
\item[Cuando] - un cliente solicita la creación del modelo para ese proxy.
\item[Entonces] - la factoría recorre la lista de modelos registrados en el sistema y crea
  una instancia de cada uno de ellos que sea compatible con el proxy.
\end{description}

De la misma manera que cada objeto o dispositivo es único en una red distribuida, los
modelos compatibles que lo representen internamente en el sistema también han de serlo (un
modelo por cada interfaz diferente). Por tanto, la Factoría de modelos debe llevar un
control de los modelos ya creados para distintos objetos, de manera que si un nuevo
cliente se interesa en trabajar con él, el modelo con el que interactúe sea el mismo que
utilizan el resto de usuarios.

\begin{description}
\item[Dado] - un proxy a un objeto que ya está representado en el sistema.
\item[Cuando] - un cliente solicita la creación del modelo para ese proxy.
\item[Entonces] - la factoría devuelve el mismo modelo.
\end{description}

\section{Consulta y modificación del estado de un objeto}
\label{sec:pruebas:consulta_modificacion_estado}

Los objetos o dispositivos físicos que componen la red distribuida deben recibir las
invocaciones de los modelos que los representan para consultar o modificar su estado.

\subsection{Consulta del estado}
\label{sec:pruebas:consulta_modificacion_estado:consulta}

Una vez creado un modelo para un dispositivo, se crean pruebas que determinen que las
invocaciones sobre los modelos se traducen en invocaciones al dispositivo remoto.

\begin{description}
\item[Dado] - un modelo de un objeto.
\item[Cuando] - se invoca su método get.
\item[Entonces] - se obtiene el mismo valor que presenta el dispositivo en ese momento.
\end{description}

\subsection{Modificación del estado}
\label{sec:pruebas:consulta_modificacion_estado:modificacion}

De manera análoga a la anterior, debe comprobarse que cuando se invoca la función
pertinente de un modelo para establecer un nuevo estado, esta invocación sea recibida por
el objeto remoto.

\begin{description}
\item[Dado] - un modelo de un objeto.
\item[Cuando] - se invoca su método set con un valor concreto.
\item[Entonces] - el dispositivo recibe la invocación y modifica su estado con el valor
  asignado en el set.
\end{description}

\section{Creación e invocación del \emph{Listener} en objetos activos}
\label{sec:pruebas:listener}

Tal como se explicó en la sección \ref{sec:desarrollo:implementacion:objetos_activos}, los
modelos de los objetos activos cuentan con un \emph{Listener} subscrito al observador del
dispositivo remoto, de manera que cuando éste modifica su estado invoca a este
\emph{Listener} para que notifique al controlador o controladores dicho cambio.

\subsection{Invocación del  \emph{Listener} por un objeto activo}
\label{sec:pruebas:listener:invocacion}

Se comprueba que cuando un objeto activo cambia su estado, el \emph{Listener} recibe una
invocación con ese cambio.

\begin{description}
\item[Dado] - un modelo de un objeto y su listener creado y subscrito al observador del
  objeto.
\item[Cuando] - de manera asíncrona el dispositivo cambio de estado.
\item[Entonces] - el listener recibe una invocación con ese cambio.
\end{description}

\subsection{Comprobación de que el \emph{Listener} es un \emph{singleton}}
\label{sec:pruebas:listener:singleton}

Dado que un modelo de un objeto es un singleton, el listener asociado a él debe ser
también una instancia única.

\begin{description}
\item[Dado] - un modelo de un objeto y su listener creado y subscrito.
\item[Cuando] - se intenta crear un nuevo listener.
\item[Entonces] - el modelo devuelve al usuario la misma instancia creada anteriormente.
\end{description}

\subsection{Invocación del controlador por parte del \emph{Listener}}
\label{sec:pruebas:listener:controller}

Cuando un \emph{Listener} recibe una invocación, su cometido es notificarlo al controlador
o controladores del objeto para que actualicen las vistas.

\begin{description}
\item[Dado] - un modelo de un objeto y su listener creado y subscrito.
\item[Cuando] - de manera asíncrona el dispositivo cambio de estado.
\item[Entonces] - se comprueba que todos los controladores del modelo han recibido la
  notificación del cambio.
\end{description}

\section{Obtención y asignación de un nuevo observador en objetos activos}
\label{sec:pruebas:observer}

El Modelo Activo permite consultar el Observador de un objeto y asignarle uno nuevo. Se
comprueba que tanto la consulta como la asignación se realizan correctamente.

\subsection{Obtención del Observador}
\label{sec:pruebas:observador:obtencion}

\begin{description}
\item[Dada] - una instancia del modelo activo de un objeto.
\item[Cuando] - se invoca su método getObserver().
\item[Entonces] - el proxy obtenido es el mismo que el del observador del propio objeto.
\end{description}

\subsection{Asignación de un nuevo Observador}
\label{sec:pruebas:observador:asignacion}

\begin{description}
\item[Dada] - una instancia del modelo activo de un objeto.
\item[Cuando] - se invoca su método setObserver(proxy) con el proxy de un objeto que
  implementa sus mismas interfaces.
\item[Entonces] - se comprueba, realizando un get, que el observador es el mismo que acaba de
  asignarse.
\end{description}

\section{Inspección y modificación de un objeto contenedor}
\label{sec:pruebas:container}

En la sección \ref{sec:desarrollo:implementacion:container} se describieron los Objetos
Contenedores. A continuación se describen las pruebas realizadas para este tipo especial
de objetos compuestos.

\subsection{Obtención de los hijos de un contenedor}
\label{sec:pruebas:container:hijos}

Se realizan pruebas que garanticen que se obtienen todos los elementos que conforman el
contenedor desde las vistas.

\begin{description}
\item[Dado] - un objeto contenedor y una instancia del modelo que lo representa.
\item[Cuando] - se invoca el método list() del modelo.
\item[Entonces] - la lista obtenida debe coindicidir con los nodos hijos del objeto
  remoto.
\end{description}

\subsection{Creación de un nuevo contenedor dentro de otro}
\label{sec:pruebas:container:create}

El Modelo Contenedor debe permitir la creación de contendores dentro de él mismo.

\begin{description}
\item[Dado] - un objeto contenedor y una instancia del modelo que lo representa.
\item[Cuando] - se invoca el método create(Nombre) para crear un nodo hijo.
\item[Entonces] - se comprueba que el nodo se ha insertado dentro de la lista de hijos y
  que este hijo pertence al modelo contenedor.
\end{description}

\subsection{Enlace de un objeto a un contenedor}
\label{sec:pruebas:container:link}

Los contenedores están compuestos por un grupo de objetos que se asocian a él como
hijos. Se comprueba que el modelo contenedor permite la inclusión de más obetos en su
lista de nodos hijos.

\begin{description}
\item[Dado] - un objeto contenedor y una instancia del modelo que lo representa.
\item[Cuando] - se invoca el método link(proxy) con el proxy de un objeto de cualquier
  tipo que pertenece al sistema.
\item[Entonces] - se comprueba que el nodo se ha insertado dentro de la lista de hijos.
\end{description}

\subsection{Desenlazar un objeto de un contenedor}
\label{sec:pruebas:container:unlink}

Los objetos que forman parte de un contenedor pueden dejar de hacerlo sin necesidad de que
desaparezcan del sistema.

\begin{description}
\item[Dado] - un objeto contenedor y el proxy de uno de sus nodos hijo.
\item[Cuando] - se invoca el método unlink(nombre, proxy) con el nombre y el proxy de un
  objeto de cualquier tipo que pertenece su lista de hijos.
\item[Entonces] - se comprueba que el nodo se ha eliminado de la lista de hijos.
\end{description}

\subsection{Destruir un objeto contenedor}
\label{sec:pruebas:container:destroy}

Cuando se destruye un objeto contenedor desaparece del sistema.

\begin{description}
\item[Dado] - una instancia de un objeto contenedor hijo.
\item[Cuando] - se invoca el método destroy() de la propia instancia.
\item[Entonces] - al comprobar si ese objeto está incluido en la lista de nodos hijos de
  su padre, se lanza una excepción porque el objeto ya no existe.
\end{description}

\section{Gestión de canales con el modelo Topic Manager}
\label{sec:pruebas:topicmanager}

El \emph{Topic Manager} es el gestor de canales proporcionado por \emph{IceStorm}. Se
detallan a continuación las pruebas realizadas para la construcción de su modelo en el
sistema.

\subsection{Listado de los canales registrados}
\label{sec:pruebas:topicmanager:listado}

Para comprobar que el modelo es capaz de obtener todos los canales registrados en el Topic
Manager, y así poder listarlos en la vista correspondiente se realiza la siguiente
prueba.

\begin{description}
\item[Dado] - la instancia del modelo Topic Manager, se crean varios canales con el método create(nombre).
\item[Cuando] - se realiza una llamada al método retrieveAll().
\item[Entonces] - la lista de canales obtenidos coincide con la de canales creados previamente.
\end{description}

\subsection{Creación y listado de un nuevo canal}
\label{sec:pruebas:topicmanager:create}

Si durante la interacción del usuario con el sistema se registra algún nuevo canal en el
Topic Manager, este debe ser capaz de listarlo junto a los canales ya registrados
previamente.

\begin{description}
\item[Dado] - la instancia del modelo Topic Manager, se crea un canal con el método
  create('Nombre').
\item[Cuando] - se realiza una llamada al método retrieve('Nombre').
\item[Entonces] - el resultado de la consula es afirmativo.
\end{description}

\subsection{Petición de un canal que no existe}
\label{sec:pruebas:topicmanager:noexist}

El sistema debe ser capaz de avisar cuando se solicite un canal que no existe, de la misma
manera que actúa cuando se solicita un objeto que no existe.

\begin{description}
\item[Dado] - la instancia del modelo Topic Manager.
\item[Cuando] - se realiza una llamada al método retrieve('Nombre'), solicitando un canal
  que no existe.
\item[Entonces] - el resultado de la consula es un mensaje de error.
\end{description}

\section{Eventos en el canal de anunciamientos}
\label{sec:pruebas:anunciamientos}

El Canal de Anunciamientos es un tipo de canal especial, como ya se describió en la
sección \ref{sec:desarrollo:implementacion:canal_anunciamientos}. Básicamente es un modelo
más de objeto activo, por tanto las pruebas se realizan sobre su \emph{Listener} para
comprobar que el sistema notifica correctamente los anunciamientos al controlador.

\begin{description}
\item[Dado] - la instancia del modelo del canal de anunciamientos.
\item[Cuando] - un objeto externo se anuncia mediante la invocación al método \emph{adv}.
\item[Entonces] - el listener recibe una invocación con ese anunciamiento y lo notifica a
  los controladores.
\end{description}

\section{Peticiones al Servidor}
\label{sec:pruebas:servidor}

El Servidor, parte central del sistema, es que el que recibe todas las peticiones desde
las vistas. Debe ser capaz de atender todas ellas correctamente, y delegar el
procesamiento de los mensajes que lleguen mediante los controladores para invocar a los
modelos y modificar las vistas según corresponda.

\subsection{Petición de la página principal}
\label{sec:pruebas:servidor:principal}

Cuando el servidor reciba una petición sin nigún tipo de argumento, debe devolver al
usuario la página principal para que pueda comenzar a trabajar desde ella.

\begin{description}
\item[Dada] - una petición \acs{HTTP} compuesta únicamente por la dirección \acs{IP} del
  servidor y el puerto en el que escucha.
\item[Cuando] - el servidor recibe la petición.
\item[Entonces] - se comprueba que la página enviada de vuelta corresponde con la
  principal.
\end{description}

\subsection{Petición de la vista de un objeto}
\label{sec:pruebas:servidor:peticion}

El usuario, si conoce el proxy del objeto, puede solicitar su vista introduciendo la
\acs{URL} bien formada de manera manual en el navegador, o mediante el textbox habilitado
para ello en la página principal. Si el proxy corresponde a un objeto, el servidor
devolverá la vista asociada por defecto al cliente.

\begin{description}
\item[Dada] - una url bien formada con todos sus argumentos (el proxy a un objeto, el
  modelo y la vista).
\item[Cuando] - el servidor recibe y procesa la petición.
\item[Entonces] - se comprueba que la página enviada de vuelta corresponde con la vista
  asociada a ese modelo.
\end{description}

\subsection{Petición mal formada}
\label{sec:pruebas:servidor:url_mal}

Si el usuario introduce una \acs{URL} mal formada, el servidor debe responder con un
mensaje que avise de este hecho.

\begin{description}
\item[Dada] - una \acs{URL} mal formada con ausencia de alguno o todos los argumentos.
\item[Cuando] - el servidor recibe y procesa la petición.
\item[Entonces] - se comprueba que la página enviada de vuelta contiene el mensaje de
  error que informa del fallo.
\end{description}

\subsection{Petición de un proxy mal formado}
\label{sec:pruebas:servidor:proxy_mal}

Puede ocurrir que la \acs{URL} de peticón esté bien formada y cuente con todos los
argumentos, pero que el proxy no esté bien formado o no corresponda a ningún objeto. En
este caso el servidor, como en el caso anterior, debe avisar al usuario de que el
dispositivo no se encuentra activo o el proxy es erróneo.

\begin{description}
\item[Dada] - una \acs{URL} bien formada pero con un proxy erróneo.
\item[Cuando] - el servidor recibe y procesa la petición.
\item[Entonces] - se comprueba que la página enviada de vuelta contiene el mensaje de
  error que informa del fallo.
\end{description}

\subsection{Petición de cambio de vista}
\label{sec:pruebas:servidor_cambio_vista}

Ya se ha explicado que para un mismo modelo el sistema cuenta con diferentes vistas que el
usuario puede solicitar en cualquier momento. El cambio se realiza mediante la selección
en una lista de la nueva vista. El servidor, cuando reciba la nueva petición, devolverá la
vista solicitada.

\begin{description}
\item[Dada] - una \acs{URL} bien formada a partir de la petición de un cambio de vista
  desde el navegador.
\item[Cuando] - el servidor recibe y procesa la petición.
\item[Entonces] - se comprueba que la página enviada de vuelta corresponde con la vista
  solicitada en la url.
\end{description}

\subsection{Petición de cambio de modelo}
\label{sec:pruebas:servidor:cambio_modelo}

De manera análoga a la anterior, el usuario puede solcitar un cambio de modelo en
cualquier momento desde el navegador, seleccionando el nuevo en una lista. Se realizará
por tanto una nueva petición al servidor que deberá responder con la vista por defecto del
nuevo modelo solicitado.

\begin{description}
\item[Dada] - una \acs{URL} bien formada a partir de la petición de un cambio de modelo
  desde el navegador.
\item[Cuando] - el servidor recibe y procesa la petición.
\item[Entonces] - se comprueba que la página enviada de vuelta corresponde con el modelo
  solicitado y su vista por defecto.
\end{description}


\section{Pruebas de integración}
\label{sec:pruebas:integracion}
Por último se realiza una prueba de integración donde intervienen todos los componentes
que integran el sistema.

En esta prueba intervienen los siguientes componentes:

\begin{itemize}
\item Dos dummies que representan dos dispositivos que implementan las interfaces
  \acs{DUO} del módulo IBool y del módulo Active (ver Anexo \ref{sec:duo}).
\item Un dummy que representa un dispositivo que implementa las interfaces \acs{DUO} del
  módulo IByte y del módulo Active.
\item El servidor web.
\item Varios clientes que modifican el estado de los dummies asíncronamente.
\item El navegador web Chromium.
\end{itemize}

El objetivo de esta prueba es comprobar que al interactuar directamete desde el navegador,
los scripts de las diferentes páginas web trabajan correctamente enviando los datos
pertinentes al servidor, y que el sistema los procesa correctamente hasta hacerlos llegar
a los dispositivos finales. Se comprueba que los cambios de estado desde la vista de un
modelo booleano y de un modelo byte llegan a sus dispositivos, y además se realiza una
prueba de establecimiento de un nuevo observador a uno de los dispositivos booleanos,
añadiéndole desde la vista el proxy al otro dispositivo de prueba booleano, y comprobando
después que este proceso se ha realizado con éxito.

Del mismo modo, se comprueba que el funcionamiento en sentido inverso también es el
esperado, modificando asíncronamente los estados de los dispositivos y comprobando que
estos cambios se traducen en actualizaciones de los elementos en las vistas pertinentes en
el lado del navegador.
