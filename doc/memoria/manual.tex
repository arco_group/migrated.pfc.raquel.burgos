\chapter{Manual de usuario}
\label{cha:manual}

En esta sección se describirán las diferentes opciones disponibles para el usuario, desde
el acceso a la página web principal, hasta las diferentes posibilidades de inspección e
interacción que le ofrece el sistema y cómo puede llevarlas a cabo.

 \section{Configuración y arranque del servidor}
 \label{sec:manual:configuracion}

 % Para que el servidor funcione correctamente, previamente se debe haber puesto en marcha
 % el servicio IceStorm. Además, el sistema requiere los Slices de \acs{DUO} para poder
 % crear y trabajar con los modelos, por lo que es requisito imprescindible que la máquina
 % donde vaya a ponerse en marcha el servidor, tenga instalados los siguientes paquetes:

Antes de poner en marcha el servidor, hay que tener en cuenta los siguientes requisitos.

\subsection{Paquetes necesarios}
\label{sec:manual:configuracion:paquetes}

Para la instalación y configuración se necesitan los siguientes paquetes:

\begin{itemize}
\item zeroc-ice
\item istaf
\item duo
\item libcontextservice
\end{itemize}

Los primeros paquetes son necesarios para lanzar correctamente los servicios IceStorm e
Istaf, útiles en el sistema para gestionar todos los canales de eventos entre modelos y
dispositivos.

Para la creación de los modelos, el sitema necesita los Slices de \acs{DUO}, configurados
tras la instalación del paquete que lleva el mismo nombre. Además, la librería
\emph{libcontextservice} da soporte a la creación de contenedores de objetos.

\subsection{Arranque del servidor}
\label{sec:manual:configuracion:arranque}

Tras la instalación de todos los paquetes necesarios, para poner en marcha el servidor hay
que llevar a cabo la siguiente secuencia de operaciones.

Poner en marcha los servicios IceStorm e Istaf es tan simple como ejecutar la siguiente sentencia:

\begin{listing}
$ duo-istaf
\end{listing}%$

A continuación se debe lanzar el servicio Icebox, al que se deberá facilitar el archivo de
configuración requerido para que el servicio ContextService trabaje adecuadamente. Desde
el directorio raíz del proyecto:

\begin{listing}
$ icebox --Ice.Config=config/icebox-contextService.cfg
\end{listing}%$

Por último, desde ese mismo directorio, se arranca el servidor:

\begin{listing}
$ python inspectio/server.py
\end{listing}%$

A partir de aquí el sistema está en marcha y el servidor escucha en el puerto 8090 de la
máquina, a la espera de las peticiones por parte del usuario desde su navegador.

\section{Acceso remoto al sistema}
\label{sec:manual:acceso}

Para acceder al sistema de manera remota, el usuario simplemente ha de contar con un
dispositivo con conexión a internet y un navegador web que soporte WebSocket (Chrome,
Safari, Mozilla, etc.).

El sistema, que estará configurado y arrancado, será accesible en su dirección \acs{IP} y
en el puerto 8090. Por tanto, para obtener la página de inicio, el usuario simplemente
tendrá que escribir en la barra de dirección de su navegador esta \acs{IP} seguida de dos
puntos y el puerto.

\section{Inspección de un dispositivo remoto}
\label{sec:manual:inspeccion}

Si el usuario conoce el proxy a un objeto remoto particular, bastará con escribirlo en el
textbox de la parte superior de la página y hacer clic en botón \emph{Search} (imagen \ref{fig:newProxy}), para que el
sistema le devuelva el modelo y vista por defecto para dicho objeto. En el caso de que el
proxy contenga algún error, o que el sistema sea incapaz de encontrar el
dispositivo o no pueda acceder a él en un momento determinado, avisará al usuario con un
mensaje de error.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{images/newProxy.png}
  \caption{Búsqueda de un nuevo dispositivo por proxy}
  \label{fig:newProxy}
\end{figure}

Si todo ha ido bien, se añadirá una pestaña a cada uno de los paneles que conforman la
página con el nombre o identidad del objeto. Haciendo clic sobre ellas se desplegará la
vista devuelta por el servidor.

% Por otro lado, como se explicó en la sección \ref{sec:desarrollo:implementacion}, si el
% usuario conoce la \acs{URL} bien formada de acceso a un dispositivo con todos sus parámet

\subsection{Objetos booleanos}
\label{sec:manual:inspeccion:bool}

La Vista por defecto de un objeto booleano cuenta con un checkbox en el centro del panel
que mostrará el estado actual del objeto (imagen \ref{fig:booleanViews} izquierda). Para
cambiar el estado, el usuario simplemente ha de hacer clic sobre él. Cuando el dispositivo
cambie su estado por motivos ajenos, el sistema actualizará automáticamente la vista.


\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/booleanViews.png}
  \caption{Distintas vistas para un objeto booleano}
  \label{fig:booleanViews}
\end{figure}

En la parte inferior de la imagen \ref{fig:booleanViews}, se pueden apreciar dos listas
desplegables diferentes. La primera sirve para cambiar el modelo con el que se desea
trabajar, y la segunda para cambiar el tipo de vista para el mismo modelo. Simplemente con
seleccionar un elememento de la lista diferente al actual, el sistema cargará en el panel
en que se esté interactuando la vista solicitada.

El funcionamiento de las distintas vistas es idéntico aunque el elemento con el que
interactúe el usuario sea diferente. En la parte derecha de la imagen \ref{fig:booleanViews}
se muestra una vista alternativa para el modelo booleano, que presenta un switch donde se
puede cambiar el estado seleccionando cualquiera de las dos posibilidades (Enable o
Disable).

\subsection{Objetos enteros}
\label{sec:manual:inspeccion:byte}

Un objeto entero cuenta con un estado o valor numérico en un rango de 0 a 255. Por tanto,
la vista de este tipo de objetos permite conocer este valor o modificarlo introduciendo
uno nuevo y pulsando \emph{Enter} o el botón \emph{Validate}. En la imagen
\ref{fig:byteViews} se muestran dos vistas diferentes para este tipo de dispositivos.


\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/byteViews.png}
  \caption{Distintas vistas para un objeto entero}
  \label{fig:byteViews}
\end{figure}

De manera análoga a los objetos booleanos, desde las vistas de los objetos enteros se
puede cambiar el modelo o la vista seleccionando la opción deseada de alguna de las
listas desplegables inferiores.

\subsection{Contenedores}
\label{sec:manual:inspeccion:container}

Los contenedores de objetos se muestran en el sistema como un árbol, donde el nodo raíz es
el propio contenedor, y sus hijos son los elementos que lo forman (panel derecho de la
figura \ref{fig:containerView}). Desde esta vista, el usuario tiene la posibilidad de
cargar la vista de un objeto hijo simplemente haciendo clic sobre su nombre. Este hijo,
como el resto de objetos, se añadirá a la lista de pestañas y su vista estará disponible
en ambos paneles principales. Por tanto, conociendo el proxy a un objeto de este tipo, el
usuario tiene acceso a todos los elementos que lo forman sin necesidad de conocer nada de
ellos.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/containerView.png}
  \caption{Vista de un objeto booleano (izquierda) y un contenedor (derecha)}
  \label{fig:containerView}
\end{figure}

Además de cargar nuevos objetos desde la vista de un contenedor, el usuario tiene la
posibilidad de añadir hijos al mismo arrastrando el proxy del objeto seleccionado y
soltándolo sobre el contenedor. En la imagen \ref{fig:containerView} se puede ver esta
funcionalidad en proceso, mientras que en la imagen \ref{fig:container2} se observa el
resultado.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/containerView2.png}
  \caption{Objeto booleano añadido al contenedor de la derecha}
  \label{fig:container2}
\end{figure}

\subsection{Objetos activos}
\label{sec:manual:inspeccion:activos}

Los objetos activos se cargan en la página principal con el modelo por defecto asignado
por el sistema. Para poder consultar el observador de estos objetos o asignarle uno nuevo,
el usuario debe seleccionar el ActiveModel de la lista desplegable de modelos. La vista
que obtendrá del objeto es la que aparece en la figura
\ref{fig:activeView}.

%meter aqui imagen de la vista de activos

Para establecer un nuevo observador al objeto, basta con introducir el proxy de este nuevo
observador en el textbox central (manualmente o arrastrándolo desde otro panel) y pulsar
el botón \emph{Validate}.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{images/activeView.png}
  \caption{Vista del Modelo Activo}
  \label{fig:activeView}
\end{figure}

\subsection{Propiedades generales de un objeto}
\label{sec:manual:inspeccion:object}

Si el usuario desea conocer las propiedades específicas de un objeto determinado, como por
ejemplo las interfaces que implementa, debe seleccionar el ObjectModel de la lista
correspondiente. Se cargará entonces en el panel seleccionado una tabla con estas
propiedades particulares (imagen \ref{fig:objectView}).

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{images/objectView.png}
  \caption{Vista del un Modelo Objeto}
  \label{fig:objectView}
\end{figure}

\section{Acceso al Topic Manager}
\label{sec:manual:topicmanager}

El Topic Manager es el gestor de canales del sistema, y conociendo su proxy el usuario
puede acceder a toda la lista de canales activos en el sistema.Su vista cuenta con una
lista de elementos seleccionables que representa todos esos canales. Para cargar alguno de
ellos en la página principal, simplemente se ha de seleccionar el canal deseado y hacer
clic sobre el botón \emph{Load Topic} (imagen \ref{fig:topicManager-Asda} izquierda).

%meter aquí la imagen de la vista del Topic Managar

%¿Se puede subscribir un objeto a un canal arrastrando su proxy a él????
\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{images/Basic-topicManager-Asda.png}
  \caption{Acceso al TopicManager (izquierda) y al Canal de Anunciamientos (derecha)}
  \label{fig:topicManager-Asda}
\end{figure}

\section{Acceso al Canal de Anunciamientos}
\label{sec:manual:anunciamientos}

El usuario puede acceder a la vista del Canal de Anunciamientos a través de su proxy, si
lo conoce, o a través de la vista del Topic Manager (imagen
\ref{fig:topicManager-Asda}). Este canal especial se presenta en el sistema con el nombre
\emph\textbf{ASDA}. Una vez cargada su vista, el usuario podrá ir visualizando en tiempo
real los anunciamientos que se vayan registrando.

Cada dispositivo que se anuncie se añadirá a la lista seleccionable de objetos, similar a
la lista de canalas que muestra la vista del Topic Manager. Desde aquí el usuario podrá
cargar las vistas de estos dispositivos cuando se vayan registrando en el
sistema. Únicamente debe seleccionar su nombre en la lista y pulsar el botón \emph{Load
  device}.
