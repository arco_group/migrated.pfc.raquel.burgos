\documentclass[a4paper,spanish,11pt]{article}
\usepackage{custom}

\title{Herramienta interactiva para monitorización y control\\del \textit{Internet de las
  cosas}}

\begin{document}
\maketitle
\tableofcontents

\section{Introducción}

En el año 2011 más de 2.000 millones de personas se conectaron a Internet para compartir
información\footnote{\url{http://www.internetworldstats.com/stats.htm}}, aproximadamente un
tercio de la población mundial. Sin embargo, las personas no son los únicos usuarios de la
red. Ya en 2008 el número de 'cosas' conectadas a internet superó al número de personas
que habitan la Tierra. Y la tendencia es que en los próximos años esta diferencia vaya en
aumento~\cite{cisco}.


Los ejemplos de aplicación  de la integración de sensores y actuadores en red para poder
interactuar con ellos son tan dispares que van desde la ya conocida domótica doméstica,
hasta la monitorización del ganado que permita a los granjeros asegurar la buena salud de
sus animales \cite{cows}, pasando por el control de la congestión del tráfico o la
detección de incendios forestales \cite{top50applications}.


Todos estos usos de los objetos en internet están englobados dentro de la tendencia que se
conoce como el \emph{Internet de las cosas}.

\subsection{Internet of Things(IoT)}

El término \emph{Internet de las Cosas} se atribuye a Kevin Ashton para referirse a una
Internet que incluya a las personas y sus cosas identificadas con RFID
\cite{ashton}. Hasta ahora ha sido esa tecnología, junto con la de los códigos QR
\cite{qr} las que han abarcado de manera más amplia las aplicaciones desarrolladas dentro
de esta filosofía. Sin embargo, el impulso del hardware libre como la plataforma
Arduino\cite{arduino}, o la adopción del nuevo protocolo de Internet (IPv6) \cite{ipv6},
permitirán que sean cada vez más los objetos con identidad propia en la red, y que el
número de aplicaciones para identificarlos, localizarlos y trabajar con ellos se
multiplique.

De manera más específica se describen brevemente cada una de estas tecnologías.

\subsubsection{RFID}

RFID (siglas de \emph{Radio Frequency IDentification}, en español identificación por
radiofrecuencia) es un sistema de almacenamiento y recuperación de datos remoto a través
de etiquetas, similares a una pegatina, que pueden ser adheridas o incorporadas a un
producto, un animal o una persona. Un ejemplo muy ilustrativo de los usos que se le pueden
dar a estas etiquetas en el ámbito doméstico es el \emph{Violet Mir:ror}
\cite{violetmirror}, un dispositivo que lee las etiquetas pegadas en los objetos y realiza
las operaciones definidas por el propio usuario, como puede ser informar del tiempo al
detectar el paraguas, o actualizar el estado en diversas redes sociales al identificar la
taza de café. Ya en el ámbito industrial, es en la logísitica donde hasta ahora se ha
sacado el máximo provecho a esta tecnología dinamizando todos los procesos de la cadena de
sumistro de un producto \cite{logistica}.

\subsubsection{QR}

Los códigos QR (\emph{Quick Response code}, código de respuesta rápida en castellano) son
matrices de puntos bidimensionales, creadas por la empresa japonesa Denso Wave
\cite{densowave}. Estos códigos codifican urls, texto plano, ubicaciones geográficas,
etc., permitiendo que cualquier objeto porte información codificada que pueda ser leía o
modificada por cualquier persona de disponga de la aplicación necesaria en su
\emph{smartphone} o \emph{tablet}.

\subsubsection{Arduino}
\label{sec:arduino}

Arduino es una plataforma de electrónica abierta para la creación de prototipos basada en
software y hardware flexibles y fáciles de usar.  Los proyectos hechos con Arduino pueden
ejecutarse sin necesidad de conectar la placa a un ordenador, aunque tienen la posibilidad
de hacerlo y comunicarse con diferentes tipos de software (p.ej. Flash, Processing,
MaxMSP). Arduino permite crear prototipos de toda índole tecnológica a un coste reducido,
favoreciendo que el \emph{Internet de las Cosas} sea un concepto más conocido y cercano
para el público en general con proyectos tan dispares como la aplicación para smartphones
para controlar las luces de la casa \cite{cromalight}, o la máquina expendedora de
refrescos que funciona con la publicación de un ’tuit’ o con un ‘hashtag’ predeterminado
por la propia empresa de bebidas \cite{bev}.

\subsubsection{IPv6}
\label{sec:ipv6}

El Internet Protocol version 6 es la sexta versión del protocolo IP
diseñada para reemplazar a IPv4, la versión implementada en la mayoría
de dispositivos con acceso a Internet.

En febrero del año 2011 la IANA\cite{iana} asignó el último bloque
disponible de los 4,300 millones de direcciones IP proporcionadas por
IPv4. IPv6 acaba con el problema del agotamiento de espacio de
direcciones al ser capaz de proporcionar 340 sextillones de ellas, un
número más que suficiente para permitir que cada elemento susceptible
de conectarse a Internet lo haga con una dirección única.

La adopción de esta nueva versión del protocolo, en uso desde junio de 2012, permitirá que a parte
de las computadoras y los teléfonos inteligentes, sean los
electrodomésticos, la ropa, los animales o los automóviles los que
puedan identificarse de manera exclusiva en la red, impulsando de manera
significativa el \emph{Internet de las cosas} mediante el desarrollo de
nuevas aplicaciones y tecnologías.

%\emph{Internet de las cosas} abre un mundo de posibilidades en el que no sólo los
%humanos pueden beneficiarse de las telecomunicaciones. Cualquier objeto es susceptible de
%ser conectado y convertirse en comunicador interactuando con otros elementos de la red.
\subsection{Aplicaciones del IoT}

Para ilustrar el concepto de IoT, en esta sección se describen brevemente algunos
proyectos y propuestas relacionados.

\subsubsection{Smartcities: SmartSantander}

Se prevee que para el año 2025 el 70\% de la población mundial
vivirá en las ciudades. En la actualidad, las ciudades consumen el
75\% de los recursos y de la energía mundial y generan el 80\% de los
gases responsables del efecto invernadero, ocupando tan sólo el 2\%
del territorio mundial \cite{necesariasSmartCities}. Estos datos ponen
de manifiesto la necesidad de gestionar de manera eficiente y sostenible los
recursos disponibles, y es aquí donde el uso de las nuevas tecnologías
se hace imprescindible.

Se define \emph{Smart City} (en castellano Ciudad Inteligente)
como aquel núcleo urbano que usa las tecnologías de la información y
las comunicaciones para hacer que tanto su infraestructura crítica,
como sus componentes y servicios públicos ofrecidos sean más
interactivos, eficientes y los ciudadanos puedan ser más conscientes
de ellos.  Es una ciudad comprometida con su entorno, tanto desde el
punto de vista medioambiental como en lo relativo a los elementos
culturales e históricos \cite{definicionSmartCity}.

\emph{SmartSantander}\cite{smartSantander} es un proyecto de la
Universidad de Cantabria (UC) junto con Telefónica I+D que convierte
este municipio español en la primera ciudad inteligente integral de
Europa.

En septiembre de 2013 habrá distribuidos 20.000 dispositivos
inalámbricos (captadores, actuadores, cámaras, terminales
móviles,etc.) a lo largo de la ciudad. Estos dispositivos recogerán
información de todos los puntos del municipio con múltiples
aplicaciones, entre las que se encuentran las de disponer en tiempo
real de la información de las plazas libres de aparcamiento, el
análisis de los parámetros ambientales como el ruido o la emisión de
dióxido de carbono, o las necesidades de riego de los parques y
jardines.

\subsubsection{Domótica y las Smart-Homes: SmartThings}

Se entiende por \emph{Domótica} el conjunto de sistemas que permiten
la automatización de distintos elementos de un edificio con el fin de
gestionar eficientemente el consumo energético, aumentar la
seguridad, el bienestar de las personas, etc.

El término Domótica no es algo nuevo, sin embargo, hasta ahora venía
ligado a un elevado coste de implantación y mantenimiento, que lo
convertía en algo exclusivo y poco difundido. Con la
aparición de nuevos dispositivos más económicos y las nuevas
tecnologías expuestas anteriormente, cada vez son más las aplicaciones
que permiten a los usuarios crear sus propias redes de dispositivos
domóticos a un precio más asequible, como es el caso de
\emph{SmartThings}\cite{smartThings}.

La tecnología de SmartThing está basada en un router y una serie de
sensores e interruptores inteligentes que se conectan inalámbricamente
con este router y que pueden ser monitorizados a través de un conjunto
de aplicaciones para el \emph{Smartphone} del
cliente llamadas \emph{SmartApps}.

La ventaja de este producto es que cualquier cosa que el usuario tenga
en casa puede convertirse en \emph{inteligente} gracias a los diferentes kits que
proporcionan los desarrolladores. Dentro de estos kits se integran
enchufes de corriente y sensores de presencia y movimiento, pudiendo
controlar remotamente el encendido o apagado de cualquier cosa que se
pueda enchufar, controlar el estado de las puertas o ventanas, etc.,
todo ello a través de las distintas \emph{SmartApps}.

El usuario puede descargarse en su \emph{Smartphone} las aplicaciones
que más se ajusten a sus necesidades y a la configuración de sus
dispositivos o el kit adquirido.


\subsection{Representación de la información}

En un mundo en el que millones de dispositivos se conectan a internet para acceder o dar a
conocer datos obtenidos a través de sus sensores, es importante la manera en la que esos
datos se comparten y representan para convertirse en información útil para las personas.

Aplicaciones como \emph{Vista Data Vision}\cite{vdv}, \emph{Pachube}\cite{pachube} o
\emph{Tales of Things}\cite{tales}, hacen uso de \emph{Google Maps}\cite{googleMaps} para
representar gráficamente los sensores u objetos distribuidos por todo el planeta y a
través de las páginas web de cada una de ellas permiten al usuario administrar, visualizar
y analizar los datos obtenidos de los dispositivos en tiempo real.

\begin{figure}[center]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{vistadatavision.png}
    \caption{Sistema de representación de Vista Data Vision}
    \label{fig:vistadatavision}
  \end{center}
\end{figure}

A pequeña escala, cuando se trata de controlar elementos del entorno del propio usuario,
las empresas proporcionan soluciones en forma de \emph{App} para dispositivos móviles,
como es el caso de \emph{SmartThings}\cite{smartThings} para configurar y controlar una
red domótica, o el caso de \emph{Nike+}\cite{nike+}, que a través de sensores en las
zapatillas envía información a través de \emph{Bluetooth}\cite{bluetooth} al móvil para
hacer un seguimiento preciso del entrenamiento, la distancia recorrida, las calorías
quemadas, etc.

Hasta ahora las aplicaciones para el \emph{Internet de las cosas} se han centrado en
encontrar soluciones específicas para problemas concretos. La finalidad de nuestro estudio
será conseguir desacoplar los objetos de los dispositivos y tecnologías que interactuarán
con ellos, de manera que podamos configurar nuestra propia red de sensores y actuadores y
podamos acceder a cada uno de ellos remotamente a través de un navegador con independencia
del hardware o el Sistema Operativo con el que estemos trabajando.

\section{Objetivos}
El objetivo principal del proyecto es crear una herramienta flexible para la exploración
del entorno, desde la que podamos gestionar cualquier recurso accesible por red: sensores
y actuadores, y también los servicios asociados a ellos: eventos, colecciones, servicios,
etc. Se pretende poder interactuar con todos los elementos que compongan nuestra red a
través de internet, con independencia del disposivo empleado y su localización.

\subsection{Objetivos específicos}
\begin{itemize}
\item Estudio del desarrollo de aplicaciones distribuidas mediante el middleware orientado
  a objetos \emph{ZeroC Ice}.
\item Estudio y comparativa de las tecnologías existentes para la comunicación
  cliente/servidor a través de la web.
\item Aplicación web de monitorización que permita representar de manera versátil los
  objetos que conforman la red y visualizar los eventos de comunicación entre ellos.
\item Programación de un servidor genérico capaz de recibir información de los objetos
  distribuidos independientemente del controlador al que se asocien y de transmitirla al
  cliente web y viceversa.
\item Optimización de los recursos consumidos por la herramienta (ancho de banda,
  velocidad de transmisión, etc) de tal forma que permitan una comunicación eficiente.
\end{itemize}


\section{Métodos y fases de trabajo}

En primer lugar se estudiará la comunicación e interacción entre objetos distribuidos
mediante el middleware \emph{ZeroC Ice}. Este estudio nos servirá de base para continuar
el desarrollo del resto de la aplicación.

Se implementará un servidor web en Python que realice la función de enlace entre los
diferentes objetos distribuidos de nuestra red y el navegador. Será el pilar fundamental
del proyecto, ya que actuará como controlador de todos los eventos que se intercambiarán
entre los distintos componentes de la herramienta.

Llegados a este punto, se realizará un estudio comparativo de las diferentes maneras de
comunicación cliente-servidor a través de la web (AJAX, Polling, Long Polling, Websockets)
que nos permita seleccionar la tecnología que más se adecúe a nuestros objetivos de
optimización de los recursos consumidos por la herramienta.

Por último se desarrollará una interfaz gráfica (aplicación web) completamente dinámica,
para poder interactuar remotamente con los objetos registrados en la red, visualizar sus
estados, los eventos de comunicación entre ellos, el registro de nuevos elementos, etc.


\section{Medios que se pretenden utilizar}

Las herramientas y entornos de programación que se utilizarán serán Software
Libre.

\subsection{Recursos software}
\begin{itemize}
\item Python: intérprete del lenguaje de programación Python.
\item GNU Emacs: editor de textos extensible y configurable \cite{emacs}.
\item GNU Make: herramienta para la generación automática de
  ejecutables \cite{make}.
 \end{itemize}

Los lenguajes que se pretenden utilizar para la realización del
proyecto serán:

\begin{itemize}
\item Sistema Operativo: Debian Versión ustable (sid) de 32-bit.
\item Middleware \emph{ZeroC Ice}\cite{ice}.
\item Python: Lenguaje de programación orientado a objetos
  interpretado \cite{python}.
\item HTML5: Lenguaje de marcado para el desarrollo de páginas web.
\item JavaScript: Lenguaje de programación orientado a objetos interpretado.
\item Emacs: editor de texto que se utilizará para la documentación y para la
  edición de código.
\item Eclipse: entorno de desarrollo \cite{eclipse}.
\item \LaTeX: lenguaje de marcado de documentos, utilizado para
  realizar este documento y, en un futuro, la memoria del proyecto
  \cite{latex}.
\item BIB\TeX: herramienta para generar las listas de referencias
  tanto de este documento como de la memoria del proyecto.
\end{itemize}

\subsection{Recursos hardware}
\begin{itemize}
\item Procesador: Intel Core 2 Quad CPU Q6600 @ 2.40GHz x4
\item Memoria: 3,9 GiB
\end{itemize}


\pagestyle{empty}
{\small
\begin{thebibliography}{biblio}
\bibitem{cisco} The Internet of Things. How the Next Evolution of the Internet
Is Changing Everything; Dave Evans; Cisco Internet Business Solutions Group (IBSG), \url{http://www.cisco.com/web/about/ac79/docs/innov/IoT_IBSG_0411FINAL.pdf}
\bibitem{cows} Augmented business, The Economist, 2010, \url{http://www.economist.com/node/17388392}
\bibitem{top50applications} 50 Sensor Applications for a Smarter World. Get Inspired!; Libelium Comunicaciones Distribuidas S.L., 2012, \url{http://www.libelium.com/top_50_iot_sensor_applications_ranking}
\bibitem{ashton} That 'Internet of Things' Thing; Kevin Ashton \url{http://www.rfidjournal.com/article/view/4986}
\bibitem{rfid} Radio Frecuency Identification, \url{http://www.rfid.org/}
\bibitem{qr} QR Code.com, \url{http://www.qrcode.com/en/}
\bibitem{arduino} Arduino, \url{http://www.arduino.cc/es/}
\bibitem{violetmirror} Mir:ror by Violet\url{http://www.youtube.com/watch?v=lU7s1YC8ADk}
\bibitem{ipv6} IPv6.com, \url{http://www.ipv6.com/}
\bibitem{iana}The Internet Assigned Numbers Authority (IANA), \url{http://www.iana.org/}
\bibitem{vdv} Vista Data Vision, \url{http://vistadatavision.com/}
\bibitem{pachube} Cosm (Pachube), \url{https://cosm.com/}
\bibitem{tales} Tales of Things, \url{http://www.talesofthings.com/}
\bibitem{googleMaps} Google Maps, \url{https://maps.google.es/}
\bibitem{smartThings} SmartThings, \url{http://smartthings.com/}
\bibitem{nike+} Nike+, \url{http://nikeplus.nike.com/plus/}
\bibitem{bluetooth} Bluetooth, \url{http://www.bluetooth.com/Pages/Bluetooth-Home.aspx}
\bibitem{logistica} RFID: La revolución a la hora de identificar productos,  Alina Camacho Hauad, \url{http://www.revistadelogistica.com/rfid-la-revolucion-a-la-hora-de-identificar-productos.asp}
\bibitem{densowave} Denso Wave, \url{https://www.denso-wave.com/en/}
\bibitem{cromalight} Cromalight: Una App para controlar interruptores a distancia, \url{http://blog.bricogeek.com/noticias/arduino/cromalight-una-app-para-controlar-interruptores-a-distancia/}
\bibitem{bev} BEV: The Incredible Sampling Robot (BOS Ice Tea),
  \url{http://www.youtube.com/watch?v=mzUXa6JThVQ&feature=player_embedded}
\bibitem{necesariasSmartCities}¿Por qué son necesarias las Smart
  Cities?,  Ruth Gamero Tinoco,
  \url{http://blogthinkbig.com/por-que-son-necesarias-las-smart-cities/}
\bibitem{definicionSmartCity} ¿Qué es una Smart City?, Fundación
  Telefónica, \url{http://smartcity-telefonica.com/?p=373}
\bibitem{smartSantander} SmartSantander, \url{http://www.smartsantander.eu/}
\bibitem{emacs} GNU Emacs manual (version 24), Free Software Foundation, 2012,
  \url{http://www.gnu.org/software/emacs/}
\bibitem{make} GNU Make Manual (version 3.82), Free Software Foundation, 28
  Julio 2010, \url{http://www.gnu.org/software/make/manual}
\bibitem{ice} Internet Communications Engine, ZeroC, \url{http://www.zeroc.com}
\bibitem{python} Python Reference Manual (version 2.7),
  Septiembre 2012, \url{http://docs.python.org/reference/index.html}
\bibitem{eclipse} The Eclipse Foundation open source community
  \url{http://www.eclipse.org}
\bibitem{latex} LaTeX, una imprenta en sus manos; Cascales Salinas, Bernardo
  y Lucas Saorín, Pascual y Mira Ros, José Manuel y Pallarés Ruiz,
  Antonio Sánchez-Pedreño Guillén, Salvador; Aula Documental de Investigación,
  2000.
\end{thebibliography}
}
\end{document}


%% Local Variables:
%%  coding: utf-8
%%  mode: latex
%%  mode: auto-fill
%%  mode: flyspell
%%  ispell-local-dictionary: "castellano8"
%% End:
