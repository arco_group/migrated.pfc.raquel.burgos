#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

import sys, os
import Ice
Ice.loadSlice("-I /usr/share/slice -I{0} --all {1} {2}".format(
        Ice.getSliceDir(),
        "/usr/share/slice/duo/DUO.ice",
        "inspectio/ContextService/ContextService.ice"))

from commodity.os_ import check_output

import ContextService

class Launcher():
    def run(self, argv):
        check_output('mkdir -p /tmp/db')
        check_output('mkdir -p /tmp/psdb')
        check_output('mkdir -p /tmp/csdb')
        print argv
        self.ic = Ice.initialize(argv)
        self.base = self.ic.propertyToProxy("ContextServer.Proxy")

        print self.base

        # Retrieve a proxy
        self.container = ContextService.ContainerPrx.checkedCast(self.base)

        child2 = self.container.create('Child3')

        child2.link('Hello1', self.ic.stringToProxy("Hello1 -t:tcp -h 127.0.0.1 -p 8889"))
        child2.link('Byte1', self.ic.stringToProxy("Byte1 -t:tcp -h 127.0.0.1 -p 8892"))
        print self.container.list()
        print child2.list()

 #       for ch in self.container.list().keys():
 #           self.container.unlink(ch)
 #       self.container.destroy()


if __name__ == "__main__":
    retval = Launcher().run(sys.argv)
    sys.exit(retval)
